
#pragma once

#include <string>
#include <vector>


using Literal = std::basic_string<unsigned int>;

class Literals {
	unsigned int size_ = 0;
	Literal vars_;
	std::vector<Literal> liters_;
public:
	void finalize() {
		if(!vars_.empty()) {
			size_ += 4 * vars_.length();
			liters_.push_back(std::move(vars_));
			vars_.clear();
		}
	}
	unsigned int add(Literal val) {
		finalize();
		unsigned int start = size_;
		size_ += 4 * val.length();
		liters_.push_back(val);
		return start;
	}
	unsigned int add(unsigned int val) {
		vars_+= val;
		return size_ + 4 * vars_.size() - 4;
	}
	
	auto begin() { return liters_.begin(); }
	auto end() { return liters_.end(); }
	unsigned int size() const { return size_; }
};
