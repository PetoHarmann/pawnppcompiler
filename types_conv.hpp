
#pragma once

#include <vector>
#include <map>
#include <optional>

#include "types.hpp"
#include "exp_val.hpp"

class TypeConversion {
public:
	virtual ~TypeConversion() noexcept = default;
	
	virtual bool isNop() const { return false; };
	virtual bool isConstexpr() const { return false; }
	
	virtual void gen() const = 0;
	virtual unsigned int convert(unsigned int value) const { throw std::runtime_error("This conversion is not constexpr."); }
};

class TypeConversionCont {
	bool implicit_;
	unsigned int toType_;
	TypeConversion* conv_;
public:
	TypeConversionCont(unsigned int toType, TypeConversion* conv, bool implicit = false)
		: implicit_(implicit), toType_(toType), conv_(conv) {}
	
	bool isImplicit() const { return implicit_; }
	unsigned int toType() const { return toType_; }
	
	TypeConversion& operator*() { return *conv_; }
	const TypeConversion& operator*() const { return *conv_; }
	TypeConversion* operator->() { return conv_; }
	const TypeConversion* operator->() const { return conv_; }
	
	TypeConversion* get() { return conv_; }
	const TypeConversion* get() const { return conv_; }
};

class NopConversion : public TypeConversion {
public:
	static NopConversion& instance() {
		static NopConversion obj;
		return obj;
	}
	
	bool isNop() const override { return true; }
	bool isConstexpr() const override { return true; }
	
	void gen() const override {}
	unsigned int convert(unsigned int value) const override{ return value; }
};

class FuncTypeConversion : public TypeConversion {
	unsigned int funcID_ = 0;
public:
	FuncTypeConversion(unsigned int id) : funcID_(id) {}
	
	//bool isConstexpr() const override { return false; }
	
	void gen() const override;
	//unsigned int convert(unsigned int value) const override;
};

class TypeConversions {
	std::vector<std::unique_ptr<TypeConversion>> manager_;
	std::map<unsigned int, std::vector<TypeConversionCont>> arr_;
	
public:
	TypeConversions() noexcept;
	
	void addConversion(unsigned int fromType, unsigned int toType, TypeConversion* conv, bool implicit = false) {
		auto it = arr_.find(fromType);
		if(it == arr_.end())
			it = arr_.emplace(fromType, std::vector<TypeConversionCont>()).first;
		it->second.emplace_back(toType, conv, implicit);
	}
	void addConversionFunc(unsigned int fromType, unsigned int toType, unsigned int id, bool implicit = false) {
		manager_.push_back(std::make_unique<FuncTypeConversion>(id));
		addConversion(fromType, toType, manager_.back().get(), implicit);
	}
	void addConversionNop(unsigned int fromType, unsigned int toType, bool implicit = false) {
		addConversion(fromType, toType, &NopConversion::instance(), implicit);
	}
	
	const TypeConversion* find(Type fromType, Type toType, bool explic = false) const {
		if(fromType.getPtrLevel() || toType.getPtrLevel())
			return nullptr;
		
		if(fromType.getID() == toType.getID())
			return &NopConversion::instance();
		
		auto it = arr_.find(fromType.getID());
		if(it == arr_.end())
			return nullptr;
		
		for(const auto & i : it->second) {
			if(i.toType() == toType.getID()) {
				if(i.isImplicit() || explic) {
					return i.get();
				}
				else {
					return nullptr;
				}
			}
		}
		return nullptr;
	}
		
	std::optional<ExpVal> tryConvert(const TypeConversion* conv, ExpVal& from, const Type& toType) const;
	std::optional<ExpVal> tryConvert(ExpVal& from, const Type& toType, bool explic = false) const { 
		return tryConvert(find(from.getType(), toType, explic), from, toType); 
	}
	ExpVal convert(const TypeConversion* conv, ExpVal& from, const Type& toType) const {
		auto ret = tryConvert(conv, from, toType);
		if(!ret)
			throw std::runtime_error("Can't convert from " + from.getType().toString() + " to " + toType.toString() + ".");
		
		return *ret;
	}
	ExpVal convert(ExpVal& from, const Type& toType, bool explic = false) const {
		return convert(find(from.getType(), toType, explic), from, toType);
	}
};



















