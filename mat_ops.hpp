
#pragma once

#include <stdexcept>

#include "compiler.hpp"
#include "opcodes.hpp"
#include "exp_val.hpp"

inline unsigned int getBinFromStr(LexemType fromType, const std::string & str) {
	if(fromType == LexemType::Int)
		return static_cast<unsigned int>(strtoll(str.c_str(), nullptr, 0));
	if(fromType == LexemType::Float)
		return ExpVal::toBin(atof(str.c_str()));
	throw std::runtime_error("Can't assign.");
}

inline void assign(Compiler & comp, ExpVal & to, ExpVal & from, bool opt = false) {
	auto & mngr = comp.getMemMngr();
	ExpVal val;
	if(!to.getType().accepts(from.getType()))
		val = comp.getConvs().convert(from, to.getType());
	else 
		val = from;
	
	if(to.getType().isReference()) {
		if(mngr.isInAlt(to, false)) {
			if(!mngr.isInPri(val, val.isReference()))
				toPri(val);
			comp.emit(Opcode::op_stor_i);
		}
		else if(mngr.isInPri(to, false)) {
			mngr.storeAlt();
			comp.emit(Opcode::op_xchg);
			if(!mngr.isInAlt(val, val.isReference()))
				toPri(val);
			comp.emit(Opcode::op_stor_i);
		}
		else {
			toPri(val);
			switch(to.getLoc()) {
				case ExpValLoc::Constexpr:
					comp.emit(Opcode::op_stor_pri, to.getCell());
					break;
				case ExpValLoc::LocalVar:
					comp.emit(Opcode::op_sref_s_pri, to.getCell());
					break;
				case ExpValLoc::GlobVar:
					comp.emit(Opcode::op_sref_pri, to.getCell());
					break;
				case ExpValLoc::Stack:
					mngr.storeAlt();
					comp.emit(Opcode::op_pop_alt);
					comp.emit(Opcode::op_stor_i);
					break;
				default:
					throw std::runtime_error("Can't assign to this ref.");
			}
		}
	}
	else if(to.getLoc() == ExpValLoc::LocalVar) {
		toPri(val);
		if(opt)
			comp.emitOp(Opcode::op_stor_s_pri, to.getCell());
		else
			comp.emit(Opcode::op_stor_s_pri, to.getCell());
	}
	else if(to.getLoc() == ExpValLoc::GlobVar) {
		toPri(val);
		if(opt)
			comp.emitOp(Opcode::op_stor_pri, to.getCell());
		else
			comp.emit(Opcode::op_stor_pri, to.getCell());
	}
	else
		throw std::runtime_error("Can't assign to this location.");
	
	mngr.setPri(to, to.isReference());
};
