
#include "local_vars.hpp"
#include "compiler.hpp"

void LocalVariables::popLayer(bool gen) {
	Compiler& comp = Compiler::instance();
	
	for(auto it = localVars_.begin(); it != localVars_.end(); ) {
		if(it->layer == layer_) {
			it->it->second.pop_back();
			if(it->it->second.empty())
				comp.getIdentifiers().erase(it->it);
			it = localVars_.erase(it);
		}
		else
			++it;
	}
	unsigned int size = oldStk_.back() - stk_;
	stk_ = oldStk_.back();
	oldStk_.pop_back();
	if(size && gen)
		comp.emit(Opcode::op_stack, size);
	--layer_;
}

int LocalVariables::addLocalVariable(std::string name, const Type& type, ExpVal & val) {
	if(type == BasicType::Void)
		throw std::runtime_error("Variable '" + name +"' can't have void type.");
	
	Compiler& comp = Compiler::instance();
		
	unsigned int id = localVars_.size();
	auto it = comp.getIdentifiers().addLocalIdent(std::move(name), IdentifierType::LocalVar, id);
		
	unsigned int size = type.getPtrLevel() ? 4 : comp.getTypeInfo(type).size; 
	stk_ -= size;
	localVars_.emplace_back(type, layer_, it, stk_);
			
	auto & mngr = comp.getMemMngr();
	if(size == 4) {
		if(val.getType() == BasicType::Void)
			comp.emit(Opcode::op_push_c, 0);
		else if(val.isConstexpr())
			comp.emit(Opcode::op_push_c, val.getCell());
		else if(mngr.isInPri(val, val.isReference()))
			comp.emitOp(Opcode::op_push_pri);
		else if(mngr.isInAlt(val, val.isReference()))
			comp.emitOp(Opcode::op_push_alt);
		else if(mngr.isInPri(val, false)) {
			comp.emit(Opcode::op_load_i);
			comp.emit(Opcode::op_push_pri);
		}
		else if(mngr.isInAlt(val, false)) {
			mngr.storePri();
			comp.emit(Opcode::op_move_pri);
			comp.emit(Opcode::op_load_i);
			comp.emit(Opcode::op_push_pri);
		}
		else {
			mngr.storePri();
			toPri(val);
			comp.emitOp(Opcode::op_push_pri);
		}
	}
	else {
		/*comp.emit(op_stack, -t.size);
		comp.emit(op_addr_alt, -t.size);
		comp.emit(op_zero_pri);
		comp.emit(op_fill, t.size);*/
	}
	return stk_;
}

int LocalVariables::addLocalArray(std::string name, const Type& type, unsigned int count) {
	Compiler& comp = Compiler::instance();
	unsigned int size = count * (type.getPtrLevel() ? 4 : comp.getTypeInfo(type).size);
	
	comp.getMemMngr().storePri();
	comp.getMemMngr().storeAlt();
	
	comp.emit(Opcode::op_zero_pri);
	comp.emit(Opcode::op_addr_alt, -size);
	comp.emit(Opcode::op_push_r, size / 4);
	stk_ -= size;
	
	Type t(type);
	t.setPtrLevel(t.getPtrLevel() + 1);
	ExpVal val = ExpVal::fromAlt(t, false);
	return addLocalVariable(std::move(name), t, val);
}

void LocalVariables::freeStack() {
	Compiler& comp = Compiler::instance();
	
	unsigned int size = oldStk_.back() - stk_;
	if(size)
		comp.emit(Opcode::op_stack, size);
}
