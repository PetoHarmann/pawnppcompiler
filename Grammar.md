



Global 		:= Type | include String | eof | pragma
Type 		:= GetType VarFunc
VarFunc 	:= identifier( FuncArgs ; | identifier( FuncArgs Func | identifier; | identifier = Expression;
FuncArgs 	:= FuncArg, ... )
FuncArg 	:= Type identifier
Func 		:= ; | Statement | Expression
Block 		:= Statement ... }
Statement 	:= { Block | IF | LocalVars | Expression; | return Expression;
LocalVars 	:= Type identifier InitValue, ...;
InitValue	:= e | = Expression
IF 			:= if(Expression) Statement | 
			   if constexpr(Expression) Statement | 
			   if(Expression) Statement else Statement | 
			   if constexpr(Expression) Statement else Statement
Expression	:= ExpMem = Expression | ExpMem operator Expression | ExpMem, Expression
ExpMem		:= literal | operator ExpMem | identifier | type(Expression) | (Expression) | func FuncCallArgs
FuncCallArgs:= (Expression, ...)
String		:= string
