
#include <iostream>

#include "types_ops.hpp"
#include "compiler.hpp"
#include "mat_ops.hpp"

using namespace std;

inline Type getAritType(const Type & a, const Type & b) {
	if(a.getPtrLevel() || b.getPtrLevel()) {
		if(a.getPtrLevel() && b.getPtrLevel() && !a.accepts(b))
			throw runtime_error("This operation does not support these operands.");
		if(a.getPtrLevel())
			return a;
		return b;
	}
	if(a == b)
		return a;
	if(a == BasicType::Int)
		return b;
	if(b == BasicType::Int)
		return a;
	throw runtime_error("Can't deduce return type.");
}

inline unsigned int getPointedSize(const Type & t) {
	assert(t.getPtrLevel());
	if(t.getPtrLevel() > 1)
		return 4;
	Compiler & comp = Compiler::instance();
	const auto & type = comp.getTypeInfo(t.getID());
	return type.size;
}

ExpVal UnaryOperator::eval(ExpVal & value) const {
	unsigned int type = value.getType().getPtrLevel() ? static_cast<unsigned int>(BasicType::Int) : value.getType().getID();
	
	Compiler & comp = Compiler::instance();
	auto it = opers_.find(type);
	ExpVal val;
	if(it == opers_.end()) {
		const TypeConversions& convs = comp.getConvs();
		const TypeConversion* conv;
		for(const auto & i : opers_) {
			conv = convs.find(value.getType(), Type(i.first));
			if(conv) {
				type = i.first;
				break;
			}
		}
		if(conv) {
			val = convs.convert(conv, value, Type(type));
			it = opers_.find(type);
		}
	}
	else
		val = value;
	
	if(it == opers_.end())
		throw runtime_error("This operation does not support this operand.");
	
	UnaryOperation & op = *it->second;
	if(op.isConstexpr() && val.isConstexpr()) {
		Type resType = op.resultType(val.getType());
		resType.addFlag(VariableFlags::Constexpr);
		return ExpVal(resType, ExpValLoc::Constexpr, op.comp(val));
	}
	
	auto & mngr = comp.getMemMngr();
	
	ExpVal ret;
	if(val.isInPri())
		ret = op.gen(val.getType());
	else if(val.isInAlt()) {
		mngr.storePri();
		ret = op.gen(val.getType(), true);
	}
	else {
		mngr.storePri();
		val.toPri();
		ret = op.gen(val.getType());
	}
	
	if(op.shouldStore())
		assign(comp, value, ret, true);
	return ret;
}

ExpVal BinaryOperator::eval(ExpVal & ia, ExpVal & ib) const {
	ExpVal a = ia, b = ib;
	Compiler & comp = Compiler::instance();
	unsigned int type1 = ia.getType().getPtrLevel() ? static_cast<unsigned int>(BasicType::Ptr) : ia.getType().getID();
	unsigned int type2 = ib.getType().getPtrLevel() ? static_cast<unsigned int>(BasicType::Ptr) : ib.getType().getID();
	
	auto it = opers_.find(make_pair(type1, type2));
	if(it == opers_.end()) {
		const TypeConversions& convs = comp.getConvs();
		const TypeConversion *conv1, *conv2;
		for(const auto & i : opers_) {
			conv1 = convs.find(a.getType(), Type(i.first.first));
			conv2 = convs.find(b.getType(), Type(i.first.second));
			
			if(conv1 && conv2) {
 				type1 = i.first.first;
				type2 = i.first.second;
				break;
			}
		}
		if(conv1 && conv2) {
			a = convs.convert(conv1, a, Type(type1));
			b = convs.convert(conv2, b, Type(type2));
			it = opers_.find(make_pair(type1, type2));
		}
	}
	
	if(it == opers_.end())
		throw runtime_error("This operation does not support these operands.");
	
	BinaryOperation & op = *it->second;
	if(op.isConstexpr() && a.isConstexpr() && b.isConstexpr()) {
		Type resType = op.resultType(a.getType(), b.getType());
		resType.addFlag(VariableFlags::Constexpr);
		return ExpVal(resType, ExpValLoc::Constexpr, op.comp(a, b));
	}
	
	auto & mngr = comp.getMemMngr();
	if(op.isPartConstexpr()) {
		if(a.isConstexpr()) {
			if(b.isInAlt())
				return op.gen(a, b.getType(), true);
			toPri(b);
			return op.gen(a, b.getType());
		}
		if(b.isConstexpr()) {
			if(a.isInAlt())
				return op.gen(a.getType(), b, true);
			toPri(a);
			return op.gen(a.getType(), b);
		}
	}
	
	if(a.isInPri()) {
		if(!b.isInAlt()) {
			toAlt(b);
			mngr.setAlt(b, b.isReference());
		}
		mngr.clrPri();
		return op.gen(a.getType(), b.getType());
	}
	if(a.isInAlt()) {
		if(!b.isInPri())
			toPri(b);
		else
			mngr.clrPri();
		return op.gen(a.getType(), b.getType(), true);
	}
	if(b.isInAlt()) {
		toPri(a);
		return op.gen(a.getType(), b.getType());
	}
	if(b.isInPri()) {
		mngr.clrPri();
		toAlt(a);
		return op.gen(a.getType(), b.getType(), true);
	}
	toPri(a);
	toAlt(b);
	mngr.setAlt(b, b.isReference());
	return op.gen(a.getType(), b.getType());
}

class UnaryOperationNeg : public UnaryOperation {
public:
	static UnaryOperationNeg* instance() {
		static unique_ptr<UnaryOperationNeg> ptr = make_unique<UnaryOperationNeg>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_neg);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & val) const override { return -val.getCell(); }
	Type resultType(const Type & t) const override { return t; }
	
	UnaryFlags flags() const override { return UnaryFlags::Constexpr; }
};

class UnaryOperationNegFloat : public UnaryOperation {
public:
	static UnaryOperationNegFloat* instance() {
		static unique_ptr<UnaryOperationNegFloat> ptr = make_unique<UnaryOperationNegFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_const_pri, 0xC0000000);
		else {
			comp.getMemMngr().storeAlt();
			comp.emit(Opcode::op_const_alt, 0xC0000000);
		}
		comp.emit(Opcode::op_xor);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & val) const override { return val.getCell() ^ 0xC0000000; }
	Type resultType(const Type & t) const override { return t; }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr; }
};

class UnaryOperationInv : public UnaryOperation {
public:
	static UnaryOperationInv* instance() {
		static unique_ptr<UnaryOperationInv> ptr = make_unique<UnaryOperationInv>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_invert);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & val) const override { return ~val.getCell(); }
	Type resultType(const Type & t) const override { return t; }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr; }
};

class UnaryOperationNot : public UnaryOperation {
public:
	static UnaryOperationNot* instance() {
		static unique_ptr<UnaryOperationNot> ptr = make_unique<UnaryOperationNot>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_not);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & val) const override { return !val.getCell(); }
	Type resultType(const Type & t) const override { return Type(BasicType::Bool); }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr; }
};

class UnaryOperationNotFloat : public UnaryOperation {
public:
	static UnaryOperationNotFloat* instance() {
		static unique_ptr<UnaryOperationNotFloat> ptr = make_unique<UnaryOperationNotFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_const_pri, ~0xC0000000);
		else {
			comp.getMemMngr().storeAlt();
			comp.emit(Opcode::op_const_alt, ~0xC0000000);
		}
		comp.emit(Opcode::op_and);
		comp.emit(Opcode::op_not);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & val) const override { return !val.getFloat(); }
	Type resultType(const Type & t) const override { return Type(BasicType::Bool); }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr; }
};

class BinaryOperationAdd : public BinaryOperation {
public:
	static BinaryOperationAdd* instance() {
		static unique_ptr<BinaryOperationAdd> ptr = make_unique<BinaryOperationAdd>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emitOp(Opcode::op_add);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() + b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationAddFloat : public BinaryOperation {
public:
	static BinaryOperationAddFloat* instance() {
		static unique_ptr<BinaryOperationAddFloat> ptr = make_unique<BinaryOperationAddFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emitOp(Opcode::op_push_alt);
		comp.emitOp(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatadd);
		comp.emit(Opcode::op_stack, 12);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() + b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Float); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationAddPtr : public BinaryOperation {
public:
	static BinaryOperationAddPtr* instance() {
		static unique_ptr<BinaryOperationAddPtr> ptr = make_unique<BinaryOperationAddPtr>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(!a.getPtrLevel()) {
			if(rev)
				comp.emit(Opcode::op_xchg);
			if(getPointedSize(b) == 4)
				comp.emit(Opcode::op_idxaddr);
			else {
				comp.emit(Opcode::op_smul_c, getPointedSize(b));
				comp.emitOp(Opcode::op_add);
			}
		}
		else if(!b.getPtrLevel()) {
			if(!rev)
				comp.emit(Opcode::op_xchg);
			if(getPointedSize(a) == 4)
				comp.emit(Opcode::op_idxaddr);
			else {
				comp.emitOp(Opcode::op_add);
				comp.emit(Opcode::op_smul_c, getPointedSize(a));
			}
		}
		else
			comp.emitOp(Opcode::op_add);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	ExpVal gen(const Type & a, const ExpVal & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(!a.getPtrLevel()) {
			if(rev)
				comp.emit(Opcode::op_move_pri);
			comp.emit(Opcode::op_smul_c, getPointedSize(b.getType()));
			comp.emit(Opcode::op_add_c, b.getCell());
		}
		else if(!b.getType().getPtrLevel()) {
			if(rev)
				comp.emit(Opcode::op_move_pri);
			comp.emitOp(Opcode::op_add_c, getPointedSize(a) * b.getCell());
		}
		else 
			comp.emitOp(Opcode::op_add_c, b.getCell());
		return ExpVal::fromPri(resultType(a, b.getType()), false);
	}
	ExpVal gen(const ExpVal & a, const Type & b, bool rev = false) const override {
		return gen(b, a, rev);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { 
		if(a.getType().getPtrLevel() && b.getType().getPtrLevel())
			return a.getCell() + b.getCell();
		if(a.getType().getPtrLevel())
			return a.getCell() + b.getCell() * getPointedSize(a.getType());
		return a.getCell() * getPointedSize(b.getType()) + b.getCell();
	}
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr | BinaryFlags::PartConstexpr; }
};

class BinaryOperationSub : public BinaryOperation {
public:
	static BinaryOperationSub* instance() {
		static unique_ptr<BinaryOperationSub> ptr = make_unique<BinaryOperationSub>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_sub_alt);
		else
			comp.emit(Opcode::op_sub);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() - b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationSubFloat : public BinaryOperation {
public:
	static BinaryOperationSubFloat* instance() {
		static unique_ptr<BinaryOperationSubFloat> ptr = make_unique<BinaryOperationSubFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev) {
			comp.emitOp(Opcode::op_push_pri);
			comp.emitOp(Opcode::op_push_alt);
		}
		else {
			comp.emitOp(Opcode::op_push_alt);
			comp.emitOp(Opcode::op_push_pri);
		}
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatsub);
		comp.emit(Opcode::op_stack, 12);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() - b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Float); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationSubPtr : public BinaryOperation {
public:
	static BinaryOperationSubPtr* instance() {
		static unique_ptr<BinaryOperationSubPtr> ptr = make_unique<BinaryOperationSubPtr>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(!a.getPtrLevel()) {
			if(rev)
				comp.emit(Opcode::op_xchg);
			comp.emit(Opcode::op_smul_c, getPointedSize(a));
			comp.emit(Opcode::op_sub);
		}
		else if(!b.getPtrLevel()) {
			if(!rev)
				comp.emit(Opcode::op_xchg);
			comp.emit(Opcode::op_smul_c, getPointedSize(b));
			comp.emit(Opcode::op_sub_alt);
		}
		else if(!rev)
			comp.emit(Opcode::op_sub);
		else
			comp.emit(Opcode::op_sub_alt);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { 
		if(a.getType().getPtrLevel() && b.getType().getPtrLevel())
			return a.getCell() - b.getCell();
		if(a.getType().getPtrLevel())
			return a.getCell() - b.getCell() * getPointedSize(b.getType());
		return a.getCell() * getPointedSize(a.getType()) - b.getCell();
	}
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationMul : public BinaryOperation {
public:
	static BinaryOperationMul* instance() {
		static unique_ptr<BinaryOperationMul> ptr = make_unique<BinaryOperationMul>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_smul);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() * b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationMulFloat : public BinaryOperation {
public:
	static BinaryOperationMulFloat* instance() {
		static unique_ptr<BinaryOperationMulFloat> ptr = make_unique<BinaryOperationMulFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emitOp(Opcode::op_push_alt);
		comp.emitOp(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatmul);
		comp.emit(Opcode::op_stack, 12);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() * b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Float); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationDiv : public BinaryOperation {
public:
	static BinaryOperationDiv* instance() {
		static unique_ptr<BinaryOperationDiv> ptr = make_unique<BinaryOperationDiv>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.getMemMngr().clrAlt();
		if(rev)
			comp.emit(Opcode::op_sdiv_alt);
		else
			comp.emit(Opcode::op_sdiv);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() / b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationDivFloat : public BinaryOperation {
public:
	static BinaryOperationDivFloat* instance() {
		static unique_ptr<BinaryOperationDivFloat> ptr = make_unique<BinaryOperationDivFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev) {
			comp.emitOp(Opcode::op_push_pri);
			comp.emitOp(Opcode::op_push_alt);
		}
		else {
			comp.emitOp(Opcode::op_push_alt);
			comp.emitOp(Opcode::op_push_pri);
		}
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatdiv);
		comp.emit(Opcode::op_stack, 12);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() / b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Float); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationMod : public BinaryOperation {
public:
	static BinaryOperationMod* instance() {
		static unique_ptr<BinaryOperationMod> ptr = make_unique<BinaryOperationMod>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.getMemMngr().clrAlt();
		if(rev)
			comp.emit(Opcode::op_sdiv_alt);
		else
			comp.emit(Opcode::op_sdiv);
		return ExpVal::fromAlt(resultType(a, b), true);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() % b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationEq : public BinaryOperation {
public:
	static BinaryOperationEq* instance() {
		static unique_ptr<BinaryOperationEq> ptr = make_unique<BinaryOperationEq>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_eq);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() == b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationEqFloat : public BinaryOperation {
public:
	static BinaryOperationEqFloat* instance() {
		static unique_ptr<BinaryOperationEqFloat> ptr = make_unique<BinaryOperationEqFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emitOp(Opcode::op_push_alt);
		comp.emitOp(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatcmp);
		comp.emit(Opcode::op_stack, 12);
		comp.emit(Opcode::op_eq_c_pri, 0);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() == b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationNEq : public BinaryOperation {
public:
	static BinaryOperationNEq* instance() {
		static unique_ptr<BinaryOperationNEq> ptr = make_unique<BinaryOperationNEq>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_neq);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() != b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationNEqFloat : public BinaryOperation {
public:
	static BinaryOperationNEqFloat* instance() {
		static unique_ptr<BinaryOperationNEqFloat> ptr = make_unique<BinaryOperationNEqFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emitOp(Opcode::op_push_alt);
		comp.emitOp(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatcmp);
		comp.emit(Opcode::op_stack, 12);
		comp.emit(Opcode::op_eq_c_pri, 0);
		comp.emit(Opcode::op_not);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() != b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationLess : public BinaryOperation {
public:
	static BinaryOperationLess* instance() {
		static unique_ptr<BinaryOperationLess> ptr = make_unique<BinaryOperationLess>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_sgrtr);
		else
			comp.emit(Opcode::op_sless);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() < b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationLessFloat : public BinaryOperation {
public:
	static BinaryOperationLessFloat* instance() {
		static unique_ptr<BinaryOperationLessFloat> ptr = make_unique<BinaryOperationLessFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.getMemMngr().clrAlt();
		if(rev) {
			comp.emitOp(Opcode::op_push_pri);
			comp.emitOp(Opcode::op_push_alt);
		}
		else {
			comp.emitOp(Opcode::op_push_alt);
			comp.emitOp(Opcode::op_push_pri);
		}
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatcmp);
		comp.emit(Opcode::op_stack, 12);
		comp.emit(Opcode::op_zero_alt);
		comp.emit(Opcode::op_sless);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() < b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationLEq : public BinaryOperation {
public:
	static BinaryOperationLEq* instance() {
		static unique_ptr<BinaryOperationLEq> ptr = make_unique<BinaryOperationLEq>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_sgeq);
		else
			comp.emit(Opcode::op_sleq);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() <= b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationLEqFloat : public BinaryOperation {
public:
	static BinaryOperationLEqFloat* instance() {
		static unique_ptr<BinaryOperationLEqFloat> ptr = make_unique<BinaryOperationLEqFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.getMemMngr().clrAlt();
		if(rev) {
			comp.emitOp(Opcode::op_push_pri);
			comp.emitOp(Opcode::op_push_alt);
		}
		else {
			comp.emitOp(Opcode::op_push_alt);
			comp.emitOp(Opcode::op_push_pri);
		}
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatcmp);
		comp.emit(Opcode::op_stack, 12);
		comp.emit(Opcode::op_zero_alt);
		comp.emit(Opcode::op_sleq);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() <= b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationGrtr : public BinaryOperation {
public:
	static BinaryOperationGrtr* instance() {
		static unique_ptr<BinaryOperationGrtr> ptr = make_unique<BinaryOperationGrtr>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_sless);
		else
			comp.emit(Opcode::op_sgrtr);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() > b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationGrtrFloat : public BinaryOperation {
public:
	static BinaryOperationGrtrFloat* instance() {
		static unique_ptr<BinaryOperationGrtrFloat> ptr = make_unique<BinaryOperationGrtrFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.getMemMngr().clrAlt();
		if(rev) {
			comp.emitOp(Opcode::op_push_pri);
			comp.emitOp(Opcode::op_push_alt);
		}
		else {
			comp.emitOp(Opcode::op_push_alt);
			comp.emitOp(Opcode::op_push_pri);
		}
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatcmp);
		comp.emit(Opcode::op_stack, 12);
		comp.emit(Opcode::op_zero_alt);
		comp.emit(Opcode::op_sgrtr);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() > b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationGEq : public BinaryOperation {
public:
	static BinaryOperationGEq* instance() {
		static unique_ptr<BinaryOperationGEq> ptr = make_unique<BinaryOperationGEq>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_sleq);
		else
			comp.emit(Opcode::op_sgeq);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() >= b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationGEqFloat : public BinaryOperation {
public:
	static BinaryOperationGEqFloat* instance() {
		static unique_ptr<BinaryOperationGEqFloat> ptr = make_unique<BinaryOperationGEqFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.getMemMngr().clrAlt();
		if(rev) {
			comp.emitOp(Opcode::op_push_pri);
			comp.emitOp(Opcode::op_push_alt);
		}
		else {
			comp.emitOp(Opcode::op_push_alt);
			comp.emitOp(Opcode::op_push_pri);
		}
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatcmp);
		comp.emit(Opcode::op_stack, 12);
		comp.emit(Opcode::op_zero_alt);
		comp.emit(Opcode::op_sgeq);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return ExpVal::toBin(a.getFloat() >= b.getFloat()); }
	Type resultType(const Type & a, const Type & b) const override { return Type(BasicType::Bool); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationBOr : public BinaryOperation {
public:
	static BinaryOperationBOr* instance() {
		static unique_ptr<BinaryOperationBOr> ptr = make_unique<BinaryOperationBOr>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_or);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() | b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationBAnd : public BinaryOperation {
public:
	static BinaryOperationBAnd* instance() {
		static unique_ptr<BinaryOperationBAnd> ptr = make_unique<BinaryOperationBAnd>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_and);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() & b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationXor : public BinaryOperation {
public:
	static BinaryOperationXor* instance() {
		static unique_ptr<BinaryOperationXor> ptr = make_unique<BinaryOperationXor>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_xor);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() ^ b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationShl : public BinaryOperation {
public:
	static BinaryOperationShl* instance() {
		static unique_ptr<BinaryOperationShl> ptr = make_unique<BinaryOperationShl>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_shl);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() << b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationShr : public BinaryOperation {
public:
	static BinaryOperationShl* instance() {
		static unique_ptr<BinaryOperationShl> ptr = make_unique<BinaryOperationShl>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_shr);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getCell() >> b.getCell(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class BinaryOperationSShr : public BinaryOperation {
public:
	static BinaryOperationShl* instance() {
		static unique_ptr<BinaryOperationShl> ptr = make_unique<BinaryOperationShl>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & a, const Type & b, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_sshr);
		return ExpVal::fromPri(resultType(a, b), false);
	}
	unsigned int comp(const ExpVal & a, const ExpVal & b) const override { return a.getInt() >> b.getInt(); }
	Type resultType(const Type & a, const Type & b) const override { return getAritType(a, b); }
	BinaryFlags flags() const override { return BinaryFlags::Constexpr; }
};

class UnaryOperationInc : public UnaryOperation {
public:
	static UnaryOperationInc* instance() {
		static unique_ptr<UnaryOperationInc> ptr = make_unique<UnaryOperationInc>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev) {
			comp.emit(Opcode::op_inc_alt);
			return ExpVal::fromAlt(resultType(type), false);
		}
		comp.emit(Opcode::op_inc_pri);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & a) const override { return a.getInt() + 1; }
	Type resultType(const Type & t) const override { return t; }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr | UnaryFlags::Store; }
};

class UnaryOperationIncFloat : public UnaryOperation {
public:
	static UnaryOperationIncFloat* instance() {
		static unique_ptr<UnaryOperationIncFloat> ptr = make_unique<UnaryOperationIncFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emitOp(Opcode::op_push_alt);
		else
			comp.emitOp(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, ExpVal::toBin(1.0));
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatadd);
		comp.emit(Opcode::op_stack, 12);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & a) const override { return ExpVal::toBin(a.getFloat() + 1); }
	Type resultType(const Type & t) const override { return t; }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr | UnaryFlags::Store; }
};

class UnaryOperationDec : public UnaryOperation {
public:
	static UnaryOperationDec* instance() {
		static unique_ptr<UnaryOperationDec> ptr = make_unique<UnaryOperationDec>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev) {
			comp.emitOp(Opcode::op_dec_alt);
			return ExpVal::fromAlt(resultType(type), false);
		}
		comp.emitOp(Opcode::op_dec_pri);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & a) const override { return a.getInt() - 1; }
	Type resultType(const Type & t) const override { return t; }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr | UnaryFlags::Store; }
};

class UnaryOperationDecFloat : public UnaryOperation {
public:
	static UnaryOperationDecFloat* instance() {
		static unique_ptr<UnaryOperationDecFloat> ptr = make_unique<UnaryOperationDecFloat>();
		return ptr.get();
	}
	
	ExpVal gen(const Type & type, bool rev = false) const override {
		Compiler & comp = Compiler::instance();
		if(rev)
			comp.emitOp(Opcode::op_push_alt);
		else
			comp.emitOp(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, ExpVal::toBin(1.0));
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatsub);
		comp.emit(Opcode::op_stack, 12);
		return ExpVal::fromPri(resultType(type), false);
	}
	unsigned int comp(const ExpVal & a) const override { return ExpVal::toBin(a.getFloat() - 1); }
	Type resultType(const Type & t) const override { return t; }
	UnaryFlags flags() const override { return UnaryFlags::Constexpr | UnaryFlags::Store; }
};

Operators::Operators() {
	auto uOP = make_unique<UnaryOperator>();
	uOP->addOperation(BasicType::Int, UnaryOperationNeg::instance());
	uOP->addOperation(BasicType::Float, UnaryOperationNegFloat::instance());
	unary_.emplace('-', std::move(uOP));
	
	uOP = make_unique<UnaryOperator>();
	uOP->addOperation(BasicType::Int, UnaryOperationInv::instance());
	unary_.emplace('~', std::move(uOP));
	
	uOP = make_unique<UnaryOperator>();
	uOP->addOperation(BasicType::Int, UnaryOperationNot::instance());
	uOP->addOperation(BasicType::Bool, UnaryOperationNot::instance());
	uOP->addOperation(BasicType::Float, UnaryOperationNotFloat::instance());
	unary_.emplace('!', std::move(uOP));
	
	uOP = make_unique<UnaryOperator>();
	uOP->addOperation(BasicType::Int, UnaryOperationInc::instance());
	uOP->addOperation(BasicType::Float, UnaryOperationIncFloat::instance());
	unary_.emplace('0', std::move(uOP));
	
	uOP = make_unique<UnaryOperator>();
	uOP->addOperation(BasicType::Int, UnaryOperationDec::instance());
	uOP->addOperation(BasicType::Float, UnaryOperationDecFloat::instance());
	unary_.emplace('1', std::move(uOP));
	
	//===========| Binary |========================================|
	auto bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationAdd::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationAddFloat::instance());
	bOP->addOperation(BasicType::Ptr, BasicType::Ptr, BinaryOperationAddPtr::instance());
	bOP->addOperation(BasicType::Ptr, BasicType::Int, BinaryOperationAddPtr::instance());
	bOP->addOperation(BasicType::Int, BasicType::Ptr, BinaryOperationAddPtr::instance());
	binary_.emplace('+', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationSub::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationSubFloat::instance());
	bOP->addOperation(BasicType::Ptr, BasicType::Ptr, BinaryOperationSubPtr::instance());
	bOP->addOperation(BasicType::Ptr, BasicType::Int, BinaryOperationSubPtr::instance());
	bOP->addOperation(BasicType::Int, BasicType::Ptr, BinaryOperationSubPtr::instance());
	binary_.emplace('-', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationMul::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationMulFloat::instance());
	binary_.emplace('*', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationDiv::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationDivFloat::instance());
	binary_.emplace('/', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationMod::instance());
	binary_.emplace('%', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationEq::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationEqFloat::instance());
	binary_.emplace('=', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationNEq::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationNEqFloat::instance());
	binary_.emplace('0', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationLess::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationLessFloat::instance());
	binary_.emplace('<', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationLEq::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationLEqFloat::instance());
	binary_.emplace('1', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationGrtr::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationGrtrFloat::instance());
	binary_.emplace('>', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationGEq::instance());
	bOP->addOperation(BasicType::Float, BasicType::Float, BinaryOperationGEqFloat::instance());
	binary_.emplace('2', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationBOr::instance());
	binary_.emplace('5', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationBAnd::instance());
	binary_.emplace('6', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationShl::instance());
	binary_.emplace('7', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationSShr::instance());
	binary_.emplace('8', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationShr::instance());
	binary_.emplace('9', std::move(bOP));
	
	bOP = make_unique<BinaryOperator>();
	bOP->addOperation(BasicType::Int, BasicType::Int, BinaryOperationXor::instance());
	binary_.emplace('"', std::move(bOP));
}















