
#pragma once

#include <stdexcept>

#include "types.hpp"

enum class ExpValLoc {
	None,
	Constexpr,
	Stack,
	LocalVar,
	GlobVar,
	FuncArg
};

class ExpValManager;

class ExpVal {
	struct ExpValData {
		unsigned int value_;
		ExpValLoc loc_;
		unsigned int refs_ = 1;
		
		ExpValData(unsigned int value, ExpValLoc loc) : 
			loc_(loc),
			value_(value) {}
		ExpValData(unsigned int value) : 
			loc_(ExpValLoc::Constexpr),
			value_(value) {}
			
		void store(bool pri);
	};
	Type type_;
	ExpValData* data_;
	
	friend ExpValManager;
public:
	static ExpVal fromPri(const Type & t, bool deref);
	static ExpVal fromAlt(const Type & t, bool deref);
	
	static float toFloat(unsigned int val) {
		return *reinterpret_cast<const float*>(&val);
	}
	static unsigned int toBin(float val) {
		return *reinterpret_cast<const unsigned int*>(&val);
	}
	
	ExpVal() noexcept : data_(nullptr) {}
	
	ExpVal(const Type & type, ExpValLoc loc = ExpValLoc::None, unsigned int value = 0) : 
		type_(type),
		data_(new ExpValData(value, loc)) {}	
		
	ExpVal(BasicType type, unsigned int value) :
		type_(type, VariableFlags::Constexpr | VariableFlags::Const),
		data_(new ExpValData(value)) {}
		
	ExpVal(unsigned int type, unsigned int value) : 
		type_(type, VariableFlags::Constexpr | VariableFlags::Const),
		data_(new ExpValData(value)) {}
		
	explicit ExpVal(float value) :
		ExpVal(BasicType::Float, *reinterpret_cast<unsigned int*>(&value)) {}
		
	ExpVal(const Type & type, const ExpVal & o) : type_(type), data_(o.data_) {
		++data_->refs_;
	}
	
	ExpVal(const Type & type, ExpVal && o) : type_(type), data_(o.data_) {
		o.data_ = nullptr;
	}
		
	ExpVal(const ExpVal & o) noexcept : type_(o.type_), data_(o.data_) {
		++data_->refs_;
	}
	ExpVal(ExpVal && o) noexcept : type_(o.type_), data_(o.data_) {
		o.data_ = nullptr;
	}
	
	ExpVal& operator=(const ExpVal & o) {
		if(!data_ || data_->refs_ == 1)
			delete data_;
		else
			--data_->refs_;
		type_ = o.type_;
		data_ = o.data_;
		++data_->refs_;
		return *this;
	}
	
	ExpVal& operator=(ExpVal && o) {
		std::swap(type_, o.type_);
		std::swap(data_, o.data_);
		return *this;
	}
		
	~ExpVal() noexcept(false);
	
	unsigned int getCell() const { return data_->value_; }
	int getInt() const { return static_cast<int>(data_->value_); }
	float getFloat() const { return *reinterpret_cast<const float*>(&data_->value_); }
		
	const Type & getType() const { return type_; }
	ExpValLoc getLoc() const { return data_->loc_; }
	void setLoc(ExpValLoc loc) { data_->loc_ = loc; }
	
	bool isConstexpr() const { return data_->loc_ == ExpValLoc::Constexpr; }
	bool isInPri(bool deref = true) const;
	bool isInAlt(bool deref = true) const;
	bool isOnStack() const { return data_->loc_ == ExpValLoc::Stack; }
	bool isReference() const { return type_.isReference(); }
	
	void toPri(bool deref = true);
	void toAlt(bool deref = true);
	void push(bool opt = false);
	void keep(bool keep = true);
	
	void debug() const { std::cerr << "ExpVal(" << type_.toString() << ", " << data_->value_ << ", " << static_cast<int>(data_->loc_) << ")" << std::endl; }

	ExpVal getPtr();
	ExpVal deref();
};

void toPri(ExpVal & val);
void toAlt(ExpVal & val);

class ExpValManager {
	ExpVal::ExpValData* pri = nullptr;
	ExpVal::ExpValData* alt = nullptr;
	bool keepPri_ = false;
	bool keepAlt_ = false;
	bool derefPri_ = false;
	bool derefAlt_ = false;
public:
	friend std::ostream & operator<<(std::ostream & os, const ExpValManager & m) {
		return os << "(pri = " << m.pri << ", alt = " << m.alt << ")";
	}
	
	void clrPri() {
		keepPri_ = false;
		pri = nullptr;
	}
	void clrAlt() {
		keepAlt_ = false;
		alt = nullptr;
	}
	
	void storePri() {
		if(pri && keepPri_) {
			pri->store(true);
		}
		keepPri_ = false;
	}
	void storeAlt() {
		if(alt && keepAlt_) {
			alt->store(false);
		}
		keepAlt_ = false;
	}
	
	void setPri(ExpVal& val, bool deref) {
		keepPri_ = false;
		derefPri_ = deref;
		pri = val.data_;
	}
	void setAlt(ExpVal& val, bool deref) {
		keepAlt_ = false;
		derefAlt_ = deref;
		alt = val.data_;
	}
	bool isInPri(const ExpVal& val, bool deref) {
		return val.data_ == pri && derefPri_ == deref;
	}
	bool isInAlt(const ExpVal& val, bool deref) {
		return val.data_ == alt && derefAlt_ == deref;
	}
	void keepPri(bool keep = true) {
		keepPri_ = keep;
	}
	void keepAlt(bool keep = true) {
		keepAlt_ = keep;
	}
};
