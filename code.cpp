



#include "code.hpp"

using namespace std;




void Code::optimizeZero(Opcode op) {
	if(op == Opcode::op_push_pri) {
		switch(static_cast<Opcode>(insts_[lastOp_])) {
			case Opcode::op_pop_pri:
				insts_.pop_back();
				lastOp_ = prevOp_;
				prevOp_ = 0;
				memMngr_.clrPri();
				return;
			case Opcode::op_const_pri: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_push_c));
				insts_.push_back(val);
				memMngr_.clrPri();
				return;
			}
			case Opcode::op_load_s_pri: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_push_s));
				insts_.push_back(val);
				memMngr_.clrPri();
				return;
			}
			case Opcode::op_load_pri: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_push));
				insts_.push_back(val);
				memMngr_.clrPri();
				return;
			}
			case Opcode::op_addr_pri:
				insts_[lastOp_] = uint(Opcode::op_push_adr);
				memMngr_.clrPri();
				return;
			case Opcode::op_pop_alt:
			case Opcode::op_const_alt:
			case Opcode::op_load_alt:
			case Opcode::op_load_s_alt:
			case Opcode::op_push_s:
			case Opcode::op_push_c:
			case Opcode::op_push:
			case Opcode::op_push_adr:
				if(prevOp_) {
					switch(static_cast<Opcode>(insts_[prevOp_])) {
						case Opcode::op_pop_pri:
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = prevOp_;
							prevOp_ = 0;
							memMngr_.clrPri();
							return;
						case Opcode::op_const_pri: {
							uint val = insts_[prevOp_+1];
							insts_.erase(insts_.begin() + prevOp_ + 1);
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = 0;
							emit(Opcode::op_push_c, val);
							memMngr_.clrPri();
							return;
						}
						case Opcode::op_load_s_pri: {
							uint val = insts_[prevOp_+1];
							insts_.erase(insts_.begin() + prevOp_ + 1);
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = 0;
							emit(Opcode::op_push_s, val);
							memMngr_.clrPri();
							return;
						}
						case Opcode::op_load_pri: {
							uint val = insts_[prevOp_+1];
							insts_.erase(insts_.begin() + prevOp_ + 1);
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = 0;
							emit(Opcode::op_push, val);
							memMngr_.clrPri();
							return;
						}
						case Opcode::op_addr_pri:
							insts_[prevOp_] = uint(Opcode::op_push_adr);
							memMngr_.clrPri();
							return;
					}
				}
				break;
		}
	}
	else if(op == Opcode::op_push_alt) {
		switch(static_cast<Opcode>(insts_[lastOp_])) {
			case Opcode::op_pop_alt:
				insts_.pop_back();
				lastOp_ = prevOp_;
				prevOp_ = 0;
				memMngr_.clrAlt();
				return;
			case Opcode::op_const_alt: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_push_c));
				insts_.push_back(val);
				memMngr_.clrAlt();
				return;
			}
			case Opcode::op_load_s_alt: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_push_s));
				insts_.push_back(val);
				memMngr_.clrAlt();
				return;
			}
			case Opcode::op_load_alt: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_push));
				insts_.push_back(val);
				memMngr_.clrAlt();
				return;
			}
			case Opcode::op_addr_alt:
				insts_[lastOp_] = uint(Opcode::op_push_adr);
				memMngr_.clrAlt();
				return;
			case Opcode::op_pop_pri:
			case Opcode::op_const_pri:
			case Opcode::op_load_pri:
			case Opcode::op_load_s_pri:
			case Opcode::op_push_s:
			case Opcode::op_push_c:
			case Opcode::op_push:
			case Opcode::op_push_adr:
				if(prevOp_) {
					switch(static_cast<Opcode>(insts_[prevOp_])) {
						case Opcode::op_pop_alt:
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = prevOp_;
							prevOp_ = 0;
							memMngr_.clrAlt();
							return;
						case Opcode::op_const_alt: {
							uint val = insts_[prevOp_+1];
							insts_.erase(insts_.begin() + prevOp_ + 1);
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = prevOp_;
							emit(Opcode::op_push_c, val);
							memMngr_.clrAlt();
							return;
						}
						case Opcode::op_load_s_alt: {
							uint val = insts_[prevOp_+1];
							insts_.erase(insts_.begin() + prevOp_ + 1);
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = prevOp_;
							emit(Opcode::op_push_s, val);
							memMngr_.clrAlt();
							return;
						}
						case Opcode::op_load_alt: {
							uint val = insts_[prevOp_+1];
							insts_.erase(insts_.begin() + prevOp_ + 1);
							insts_.erase(insts_.begin() + prevOp_);
							lastOp_ = prevOp_;
							emit(Opcode::op_push, val);
							memMngr_.clrAlt();
							return;
						}
						case Opcode::op_addr_alt:
							insts_[prevOp_] = uint(Opcode::op_push_adr);
							memMngr_.clrAlt();
							return;
					}
				}
				break;
		}
	}
	else if(op == Opcode::op_add) {
		switch(static_cast<Opcode>(insts_[lastOp_])) {
			case Opcode::op_const_alt: {
				uint val = insts_[lastOp_+1];
				insts_.pop_back();
				insts_.pop_back();
				insts_.push_back(uint(Opcode::op_add_c));
				insts_.push_back(val);
				memMngr_.clrAlt();
				return;
			}
			case Opcode::op_pop_pri:
			case Opcode::op_const_pri:
			case Opcode::op_load_pri:
			case Opcode::op_load_s_pri:
			case Opcode::op_push_s:
			case Opcode::op_push_c:
			case Opcode::op_push:
				if(static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_const_alt) {
					uint val = insts_[prevOp_+1];
					insts_.erase(insts_.begin() + prevOp_ + 1);
					insts_.erase(insts_.begin() + prevOp_);
					lastOp_ = prevOp_;
					emit(Opcode::op_add_c, val);
					memMngr_.clrAlt();
					return;
				}
				break;
			case Opcode::op_pop_alt:
				if(static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_const_pri) {
					uint val = insts_[prevOp_+1];
					insts_.pop_back();
					insts_.pop_back();
					insts_.pop_back();
					emit(Opcode::op_pop_pri);
					emit(Opcode::op_add_c, val);
					memMngr_.clrAlt();
					return;
				}
				break;
			case Opcode::op_load_alt:
				if(static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_const_pri) {
					uint val = insts_[prevOp_+1];
					uint val2 = insts_[lastOp_+1];
					insts_.pop_back();
					insts_.pop_back();
					insts_.pop_back();
					insts_.pop_back();
					emit(Opcode::op_load_pri, val2);
					emit(Opcode::op_add_c, val);
					memMngr_.clrAlt();
					return;
				}
				break;
			case Opcode::op_load_s_alt:
				if(static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_const_pri) {
					uint val = insts_[prevOp_+1];
					uint val2 = insts_[lastOp_+1];
					insts_.pop_back();
					insts_.pop_back();
					insts_.pop_back();
					insts_.pop_back();
					emit(Opcode::op_load_s_pri, val2);
					emit(Opcode::op_add_c, val);
					memMngr_.clrAlt();
					return;
				}
				break;
		}
	}
	emit(op);
}

void Code::optimizeZero(Opcode op, uint arg1) {
	if(op == Opcode::op_stor_s_pri) {
		if(
			lastOp_ != 0 &&
			static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_load_s_pri && 
			static_cast<Opcode>(insts_[lastOp_]) == Opcode::op_inc_pri
		) {
			uint val = insts_[prevOp_+1];
			insts_.pop_back();
			insts_.pop_back();
			insts_.pop_back();
			lastOp_ = 0;
			emit(Opcode::op_inc_s, val);
			return;
		}
	}
	else if(op == Opcode::op_stor_s_alt) {
		if(
			lastOp_ != 0 &&
			static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_load_s_alt && 
			static_cast<Opcode>(insts_[lastOp_]) == Opcode::op_inc_alt
		) {
			uint val = insts_[prevOp_+1];
			insts_.pop_back();
			insts_.pop_back();
			insts_.pop_back();
			lastOp_ = 0;
			emit(Opcode::op_inc_s, val);
			return;
		}
	}
	else if(op == Opcode::op_stor_pri) {
		if(
			lastOp_ != 0 &&
			static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_load_pri && 
			static_cast<Opcode>(insts_[lastOp_]) == Opcode::op_inc_pri
		) {
			uint val = insts_[prevOp_+1];
			insts_.pop_back();
			insts_.pop_back();
			insts_.pop_back();
			lastOp_ = 0;
			emit(Opcode::op_inc, val);
			return;
		}
	}
	else if(op == Opcode::op_stor_alt) {
		if(
			lastOp_ != 0 &&
			static_cast<Opcode>(insts_[prevOp_]) == Opcode::op_load_alt && 
			static_cast<Opcode>(insts_[lastOp_]) == Opcode::op_inc_alt
		) {
			uint val = insts_[prevOp_+1];
			insts_.pop_back();
			insts_.pop_back();
			insts_.pop_back();
			lastOp_ = 0;
			emit(Opcode::op_inc, val);
			return;
		}
	}
	
	emit(op, arg1);
}





























