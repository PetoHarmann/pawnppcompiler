
#pragma once

#include <iostream>
#include <iomanip>
#include <sstream>

enum class VariableFlags : unsigned int {
	None 			= 0,
	PointerMask 	= 0xF,
	Const 			= 0x10,
	Constexpr 		= 0x20,
	Public 			= 0x40,
	Native 			= 0x80,
	ConstPtr 		= 0x100,
	VarArgs 		= 0x200,
	Reference 		= 0x400,
	AutoRef			= 0x800
};

inline VariableFlags operator~(VariableFlags a) {
	return static_cast<VariableFlags>(~static_cast<unsigned int>(a));
}
inline VariableFlags operator|(VariableFlags a, VariableFlags b) {
	return static_cast<VariableFlags>(static_cast<unsigned int>(a) | static_cast<unsigned int>(b));
}
inline VariableFlags operator&(VariableFlags a, VariableFlags b) {
	return static_cast<VariableFlags>(static_cast<unsigned int>(a) & static_cast<unsigned int>(b));
}

enum class BasicType : unsigned int {
	Void,
	Int,
	Float,
	Bool,
	Auto,
	Ptr
};

struct TypeInfo {
	std::string name;
	unsigned int size;
};

class Type {
	unsigned int type_;
	VariableFlags flags_;
public:
	Type() : type_(0u), flags_(VariableFlags::None) {}
	explicit Type(unsigned int id, VariableFlags flags = VariableFlags::None) : type_(id), flags_(flags) {}
	explicit Type(BasicType id, VariableFlags flags = VariableFlags::None) : type_(static_cast<unsigned int>(id)), flags_(flags) {}
	
	bool isConst() const { return (flags_ & VariableFlags::Const) != VariableFlags::None; }
	bool isConstPtr() const { return (flags_ & VariableFlags::ConstPtr) != VariableFlags::None; }
	bool isConstexpr() const { return (flags_ & VariableFlags::Constexpr) != VariableFlags::None; }
	bool isPublic() const { return (flags_ & VariableFlags::Public) != VariableFlags::None; }
	bool isNative() const { return (flags_ & VariableFlags::Native) != VariableFlags::None; }
	bool isVarArgs() const { return (flags_ & VariableFlags::VarArgs) != VariableFlags::None; }
	bool isAuto() const { return type_ == static_cast<unsigned int>(BasicType::Auto); }
	bool isReference() const { return (flags_ & VariableFlags::Reference) != VariableFlags::None; }
	bool isAutoRef() const { return (flags_ & VariableFlags::AutoRef) != VariableFlags::None; }
	
	Type& addFlag(VariableFlags flag) { flags_ = flags_ | flag; return *this; }
	Type& removeFlag(VariableFlags flag) { flags_ = flags_ & ~flag; return *this; }
	VariableFlags getFlags() const { return flags_; }
	
	unsigned int getID() const { return type_; }
	void setID(unsigned int id) { type_ = id; }
	
	bool operator==(BasicType type) const { return static_cast<unsigned int>(type) == type_ && (flags_ & VariableFlags::PointerMask) == VariableFlags::None; }
	bool operator!=(BasicType type) const { return !operator==(type); }
	
	bool operator==(const Type & type) const { return type.type_ == type_ && type.flags_ == flags_; }
	bool operator!=(const Type & type) const { return !operator==(type); }
	
	std::string toString() const {
		std::stringstream ss;
		ss << *this;
		return ss.str();
	}
	friend std::ostream & operator<<(std::ostream & os, const Type & t);
	
	unsigned int getPtrLevel() const { return static_cast<unsigned int>(flags_ & VariableFlags::PointerMask); }
	void setPtrLevel(unsigned int level) { flags_ = (flags_ & ~VariableFlags::PointerMask) | VariableFlags{level}; }
	
	bool accepts(const Type & t) const {
		return type_ == t.type_ && getPtrLevel() == t.getPtrLevel() && (isConstPtr() || !t.isConstPtr()); 
	}
	bool acceptsAuto(const Type & t) const {
		if(isAuto())
			return getPtrLevel() <= t.getPtrLevel() && (!t.isConstPtr() || (isConstPtr() || (getPtrLevel() == 0 && isConst())));
		return accepts(t);
	}
	void autoDeduce(const Type & t) {
		type_ = t.type_;
		setPtrLevel(t.getPtrLevel());
	}
};
