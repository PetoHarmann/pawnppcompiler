
#pragma once

#include <cstdlib>
#include <stdio.h>
#include <string>

enum class Opcode : unsigned int{
	op_none,
	op_load_pri,
	op_load_alt,
	op_load_s_pri,
	op_load_s_alt,
	op_lref_pri,
	op_lref_alt,
	op_lref_s_pri,
	op_lref_s_alt,
	op_load_i,
	op_lodb_i,
	op_const_pri,
	op_const_alt,
	op_addr_pri,
	op_addr_alt,
	op_stor_pri,
	op_stor_alt,
	op_stor_s_pri,
	op_stor_s_alt,
	op_sref_pri,
	op_sref_alt,
	op_sref_s_pri,
	op_sref_s_alt,
	op_stor_i,
	op_strb_i,
	op_lidx,
	op_lidx_b,
	op_idxaddr,
	op_idxaddr_b,
	op_align_pri,
	op_align_alt,
	op_lctrl,
	op_sctrl,
	op_move_pri,
	op_move_alt,
	op_xchg,
	op_push_pri,
	op_push_alt,
	op_push_r,
	op_push_c,
	op_push,
	op_push_s,
	op_pop_pri,
	op_pop_alt,
	op_stack,
	op_heap,
	op_proc,
	op_ret,
	op_retn,
	op_call,
	op_call_pri,
	op_jump,
	op_jrel,
	op_jzer,
	op_jnz,
	op_jeq,
	op_jneq,
	op_jless,
	op_jleq,
	op_jgrtr,
	op_jgeq,
	op_jsless,
	op_jsleq,
	op_jsgrtr,
	op_jsgeq,
	op_shl,
	op_shr,
	op_sshr,
	op_shl_c_pri,
	op_shl_c_alt,
	op_shr_c_pri,
	op_shr_c_alt,
	op_smul,
	op_sdiv,
	op_sdiv_alt,
	op_umul,
	op_udiv,
	op_udiv_alt,
	op_add,
	op_sub,
	op_sub_alt,
	op_and,
	op_or,
	op_xor,
	op_not,
	op_neg,
	op_invert,
	op_add_c,
	op_smul_c,
	op_zero_pri,
	op_zero_alt,
	op_zero,
	op_zero_s,
	op_sign_pri,
	op_sign_alt,
	op_eq,
	op_neq,
	op_less,
	op_leq,
	op_grtr,
	op_geq,
	op_sless,
	op_sleq,
	op_sgrtr,
	op_sgeq,
	op_eq_c_pri,
	op_eq_c_alt,
	op_inc_pri,
	op_inc_alt,
	op_inc,
	op_inc_s,
	op_inc_i,
	op_dec_pri,
	op_dec_alt,
	op_dec,
	op_dec_s,
	op_dec_i,
	op_movs,
	op_cmps,
	op_fill,
	op_halt,
	op_bounds,
	op_sysreq_pri,
	op_sysreq_c,
	op_file,
	op_line,
	op_symbol,
	op_srange,
	op_jump_pri,
	op_switch,
	op_casetbl,
	op_swap_pri,
	op_swap_alt,
	op_push_adr,
	op_nop,
	op_sysreq_n,
	op_symtag,
	op_break,
	op_push2_c,
	op_push2,
	op_push2_s,
	op_push2_adr,
	op_push3_c,
	op_push3,
	op_push3_s,
	op_push3_adr,
	op_push4_c,
	op_push4,
	op_push4_s,
	op_push4_adr,
	op_push5_c,
	op_push5,
	op_push5_s,
	op_push5_adr,
	op_load_both,
	op_load_s_both,
	op_const,
	op_const_s,
};

namespace Opcodes {

	typedef int cell;

	//typedef cell(*OPCODE_PROC)(FILE *ftxt, const cell *params, cell opcode, cell cip);

	/*
	cell parm0(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell parm1(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell parm2(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell parm3(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell parm4(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell parm5(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_proc(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_call(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_jump(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_sysreq(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_switch(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell casetbl(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_file(FILE *ftxt, const cell *params, cell opcode, cell cip);
	cell do_symbol(FILE *ftxt, const cell *params, cell opcode, cell cip);
	*/

	enum class ParamType {
		Cell,
		GlobVar,
		Label,
		Function,
		Native
	};

	typedef struct {
		cell opcode;
		const char *name;
		unsigned int params;
		ParamType ptype = ParamType::Cell;
	} OPCODE;

	extern const OPCODE opcodelist[];

	int getOpcode(const std::string & op);
}


