#pragma once

#include <stdio.h> 
#include <iostream>

struct amx_file_data;

class amx_file_writer {
	std::ostream & os_;
	
	template<typename T>
	inline const char * toChar(const T * v) {
		return reinterpret_cast<const char *>(v);
	}
	
	char hex(char ch){
		return ch > 9 ? 'A' + ch - 10 : '0' + ch;
	}
	
	void toHex(std::ostream & os, const char * str, unsigned int len) {
		const char * end = str + len;
		while(str != end) {
			os << hex((*str >> 4) & 0xF) << hex(*str & 0xF) << " ";
			++str;
		}
	}
public:
	amx_file_writer(std::ostream & os) : os_(os) {}

	void write(const char * data, std::streamsize size) { os_.write(data, size); }
	void write_header(const amx_file_data & data);
	
	amx_file_writer & operator<<(int val) { write(toChar(&val), sizeof(int)); return *this; }
	amx_file_writer & operator<<(unsigned int val) { write(toChar(&val), sizeof(unsigned int)); return *this; }
	amx_file_writer & operator<<(unsigned short val) { write(toChar(&val), sizeof(unsigned short)); return *this; }
	amx_file_writer & operator<<(unsigned char val) { write(toChar(&val), sizeof(unsigned char)); return *this; }
	amx_file_writer & operator<<(const std::string & val) { write(val.c_str(), val.length() + 1); return *this; }
	amx_file_writer & operator<<(const std::basic_string<unsigned int> & val) { write(reinterpret_cast<const char*>(val.data()), 4 * val.length()); return *this; }
};

static_assert(sizeof(int) == 4, "Bad size, usie int32_t please.");
static_assert(sizeof(unsigned int) == 4, "Bad size, usie int32_t please.");
static_assert(sizeof(unsigned short) == 2, "Bad size, usie int16_t please.");

struct amx_file_data {
	static constexpr unsigned int header_size = 12 * 4 + 3 * 2 + 2;
	
	unsigned int size = 0;
	unsigned short magic = 0xF1E0;
	unsigned char file_version = 8;
	unsigned char amx_version = 8;
	unsigned short flags = 0;
	unsigned short defsize = 8;

	int cod = 0;
	int dat = 0;
	int hea = 0;
	int stp = 0;
	int cip = 0;

	int publics = 0;
	int natives = 0;
	int libraries = 0;
	int pubvars = 0;
	int tags = 0;
	int nametable = 0;
	//int overlays = 0;

	amx_file_data() noexcept = default;
	//amx_file_data(FILE * f) { load(f); };

	//void load(FILE * f);
	void write(amx_file_writer & os) const;
};


/*void amx_file_data::load(FILE * f) {
	fread_s(&size, sizeof(unsigned int), 4, 1, f);
	fread_s(&magic, sizeof(unsigned short), 2, 1, f);
	fread_s(&file_version, sizeof(unsigned char), 1, 1, f);
	fread_s(&amx_version, sizeof(unsigned char), 1, 1, f);
	fread_s(&flags, sizeof(unsigned short), 2, 1, f);
	fread_s(&defsize, sizeof(unsigned short), 2, 1, f);

	fread_s(&cod, sizeof(int), 4, 1, f);
	fread_s(&dat, sizeof(int), 4, 1, f);
	fread_s(&hea, sizeof(int), 4, 1, f);
	fread_s(&stp, sizeof(int), 4, 1, f);
	fread_s(&cip, sizeof(int), 4, 1, f);

	fread_s(&publics, sizeof(int), 4, 1, f);
	fread_s(&natives, sizeof(int), 4, 1, f);
	fread_s(&libraries, sizeof(int), 4, 1, f);
	fread_s(&pubvars, sizeof(int), 4, 1, f);
	fread_s(&tags, sizeof(int), 4, 1, f);
	fread_s(&nametable, sizeof(int), 4, 1, f);
	fread_s(&overlays, sizeof(int), 4, 1, f);
}*/

void amx_file_writer::write_header(const amx_file_data & data) {
	os_.seekp(0);
	data.write(*this);
}
	
void amx_file_data::write(amx_file_writer & os) const {
	os << size;
	os << magic;
	os << file_version;
	os << amx_version;
	os << flags;
	os << defsize;
	
	os << cod;
	os << dat;
	os << hea;
	os << stp;
	os << cip;

	os << publics;
	os << natives;
	os << libraries;
	os << pubvars;
	os << tags;
	os << nametable;
	//os << overlays;
}
