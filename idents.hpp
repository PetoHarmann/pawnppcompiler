
#pragma once

#include <map>
#include <list>
#include <iostream>

enum class IdentifierType {
	Unknown,
	GlobVar,
	LocalVar,
	Func,
	Label,
	Type,
	FuncArg
};

struct Identifier {
	IdentifierType type;
	unsigned int id;
	
	Identifier(IdentifierType t, unsigned int i) : type(t), id(i) {}
};

class Identifiers {
	std::map<std::string, std::list<Identifier>> idents_;
public:
	using iterator = typename std::map<std::string, std::list<Identifier>>::iterator;
	
	iterator addGlobIdent(std::string name, IdentifierType type, unsigned int id) {
		std::list<Identifier> l;
		l.emplace_back(type, id);
		
		auto ret = idents_.emplace(std::move(name), std::move(l));
		if(!ret.second)
			throw std::runtime_error("Symbol already defined.");
		
		return ret.first;
	}
	iterator addLocalIdent(std::string name, IdentifierType type, unsigned int id);
	void erase(iterator it) { idents_.erase(it); }
	
	auto find(const std::string & name) {
		return idents_.find(name);
	}
	Identifier getIdent(const std::string & str) const {
		auto it = idents_.find(str);
		if(it == idents_.end())
			return Identifier(IdentifierType::Unknown, 0);
		return it->second.back();
	}
	
	void debug_dump() const {
		/*for(const auto & [n, l] : idents_) {
			auto i = l.back();
			if(i.type == IdentifierType::GlobVar)
				std::cout << "  GlobalVariable " << n << "(" << globVars_[i.id].type.toString() << ") = " << globVars_[i.id].init << "\n";
			else if(i.type == IdentifierType::Func)
				std::cout << "  Function " << n << "(" << funcs_[i.id].type.toString() << ", " << funcs_[i.id].args.size() <<") = " << funcs_[i.id].pos << "\n";
		}*/
	}
};



