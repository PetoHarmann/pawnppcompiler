
#pragma once

#include <map>
#include <optional>
#include <memory>
#include <cassert>

#include "exp_val.hpp"

enum class UnaryFlags : unsigned int {
	None		= 0x0,
	Constexpr  	= 0x1,
	Store		= 0x2
};

constexpr UnaryFlags operator|(UnaryFlags a, UnaryFlags b) {
	return static_cast<UnaryFlags>(static_cast<unsigned int>(a) | static_cast<unsigned int>(b));
}
constexpr UnaryFlags operator&(UnaryFlags a, UnaryFlags b) {
	return static_cast<UnaryFlags>(static_cast<unsigned int>(a) & static_cast<unsigned int>(b));
}

class UnaryOperation {
public:
	virtual ~UnaryOperation() noexcept = default;
	
	virtual ExpVal gen(const Type & type, bool rev = false) const = 0;
	virtual unsigned int comp(const ExpVal & val) const = 0;
	virtual Type resultType(const Type & type) const = 0;
	virtual UnaryFlags flags() const { return UnaryFlags::None; }
	
	bool isConstexpr() const { return (flags() & UnaryFlags::Constexpr) != UnaryFlags::None; }
	bool shouldStore() const { return (flags() & UnaryFlags::Store) != UnaryFlags::None; }
};

class UnaryOperator {
	std::map<unsigned int, UnaryOperation*> opers_;
public:
	ExpVal eval(ExpVal & val) const;
	void addOperation(unsigned int type, UnaryOperation* op) { opers_.emplace(type, op); }
	void addOperation(BasicType type, UnaryOperation* op) { addOperation(static_cast<unsigned int>(type), op); }
};

enum class BinaryFlags : unsigned int {
	None			= 0x0,
	Constexpr  		= 0x1,
	PartConstexpr  	= 0x2,
	Comutative  	= 0x4
};

constexpr BinaryFlags operator|(BinaryFlags a, BinaryFlags b) {
	return static_cast<BinaryFlags>(static_cast<unsigned int>(a) | static_cast<unsigned int>(b));
}
constexpr BinaryFlags operator&(BinaryFlags a, BinaryFlags b) {
	return static_cast<BinaryFlags>(static_cast<unsigned int>(a) & static_cast<unsigned int>(b));
}

class BinaryOperation {
public:
	virtual ~BinaryOperation() noexcept = default;
	
	virtual ExpVal gen(const Type & a, const Type & b, bool rev = false) const = 0;
	virtual ExpVal gen(const Type & a, const ExpVal & b, bool rev = false) const { throw 1; }
	virtual ExpVal gen(const ExpVal & a, const Type & b, bool rev = false) const { throw 1; }
	virtual unsigned int comp(const ExpVal & a, const ExpVal & b) const = 0;
	virtual Type resultType(const Type & a, const Type & b) const = 0;
	virtual BinaryFlags flags() const { return BinaryFlags::None; }
	
	bool isConstexpr() const { return (flags() & BinaryFlags::Constexpr) != BinaryFlags::None; }
	bool isPartConstexpr() const { return (flags() & BinaryFlags::PartConstexpr) != BinaryFlags::None; }
};

class BinaryOperator {
	std::map<std::pair<unsigned int, unsigned int>, BinaryOperation*> opers_;
public:
	ExpVal eval(ExpVal & a, ExpVal & b) const;
	void addOperation(unsigned int type1, unsigned int type2, BinaryOperation* op) { 
		opers_.emplace(std::make_pair(type1, type2), op); 
	}
	void addOperation(BasicType type1, BasicType type2, BinaryOperation* op) { 
		addOperation(static_cast<unsigned int>(type1), static_cast<unsigned int>(type2), op); 
	}
};

class Operators {
	std::map<char, std::unique_ptr<UnaryOperator>> unary_;
	std::map<char, std::unique_ptr<BinaryOperator>> binary_;
public:
	Operators();
	
	UnaryOperator& getUnary(char op) { return *unary_.at(op); }
	BinaryOperator& getBinary(char op) { return *binary_.at(op); }
};











