
#pragma once

#include <vector>
#include <cassert>

#include "opcodes.hpp"
#include "exp_val.hpp"

class Code {
	using cell = Opcodes::cell;
	using uint = unsigned int;
	
	ExpValManager memMngr_;
	
	std::vector<cell> insts_;
	std::vector<uint> jumps_;
	
	uint lastOp_ = 0;
	uint prevOp_ = 0;
public:
	Code() { emit(Opcode::op_halt, 0); }
	
	uint size() const { return insts_.size(); }
	const std::vector<cell>& getInsts() const { return insts_; }
	const std::vector<uint>& getJumps() const { return jumps_; }
	
	ExpValManager& getMemMngr() { return memMngr_; }
	const ExpValManager& getMemMngr() const { return memMngr_; }
	
	uint nextInst() const {
		return insts_.size();
	}
	uint makeLabel() {
		uint id = jumps_.size();
		jumps_.push_back(0);
		return id;
	}
	//void fillLabel(uint id, uint pos) {
	//	jumps_[id] = pos;
	//}
	void fillLabelNext(uint id) {
		jumps_[id] = insts_.size();
		lastOp_ = 0;
		prevOp_ = 0;
	}
	
	void optimizeZero(Opcode op);
	void optimizeZero(Opcode op, uint arg1);
	
	uint emit(Opcode op) {
		assert(Opcodes::opcodelist[static_cast<uint>(op)].params == 0);
		
		prevOp_ = lastOp_;
		lastOp_ = insts_.size();
		insts_.push_back(static_cast<uint>(op));
		return lastOp_;
	}
	uint emit(Opcode op, cell arg1) {
		assert(Opcodes::opcodelist[static_cast<uint>(op)].params == 1);
		
		prevOp_ = lastOp_;
		lastOp_ = insts_.size();
		insts_.push_back(static_cast<uint>(op));
		insts_.push_back(arg1);
		return lastOp_;
	}
	uint emit(Opcode op, cell arg1, cell arg2) {
		assert(Opcodes::opcodelist[static_cast<uint>(op)].params == 2);
		
		prevOp_ = lastOp_;
		lastOp_ = insts_.size();
		insts_.push_back(static_cast<uint>(op));
		insts_.push_back(arg1);
		insts_.push_back(arg2);
		return lastOp_;
	}
	void emitOp(Opcode op) {
		assert(Opcodes::opcodelist[static_cast<uint>(op)].params == 0);
		
		if(lastOp_ != 0)
			return optimizeZero(op);
		
		prevOp_ = lastOp_;
		lastOp_ = insts_.size();
		insts_.push_back(static_cast<uint>(op));
	}
	void emitOp(Opcode op, cell arg1) {
		assert(Opcodes::opcodelist[static_cast<uint>(op)].params == 1);
		
		if(lastOp_ != 0)
			return optimizeZero(op, arg1);
		
		prevOp_ = lastOp_;
		lastOp_ = insts_.size();
		insts_.push_back(static_cast<uint>(op));
		insts_.push_back(arg1);
	}
	
	Code& operator<<(const Code& o) {
		uint offset = jumps_.size();
		uint inst_offset = insts_.size();
		for(uint i : o.jumps_)
			jumps_.push_back(i == 0 ? 0 : (i - 2 + inst_offset));
		
		for(size_t i = 2, size = o.insts_.size(); i < size; ++i) {
			cell op = o.insts_[i];
			insts_.push_back(op);
			
			if(Opcodes::opcodelist[op].ptype == Opcodes::ParamType::Label) {
				insts_.push_back(o.insts_[++i] + offset);
			}
			else {
				for(size_t j = 0; j < Opcodes::opcodelist[op].params; ++j) {
					insts_.push_back(o.insts_[++i]);
				}
			}
		}
		
		memMngr_ = o.memMngr_;
		return *this;
	}
	
	void debug_dump() const {
		for(auto it = insts_.begin(); it != insts_.end(); ++it) {
			const Opcodes::OPCODE & op = Opcodes::opcodelist[*it];
			std::cout << op.name;
			if(op.params >= 1)
				std::cout << " " << *++it;
			if(op.params >= 2)
				std::cout << " " << *++it;
			std::cout << std::endl;
		}
	}
};









