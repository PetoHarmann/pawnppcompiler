
#include "exp_val.hpp"
#include "compiler.hpp"

#include <iostream>
#include <iomanip>

using namespace std;

ExpVal ExpVal::fromPri(const Type & t, bool deref) {
	ExpVal val(t);
	Compiler::instance().getMemMngr().setPri(val, deref);
	return val;
}

ExpVal ExpVal::fromAlt(const Type & t, bool deref) {
	ExpVal val(t);
	Compiler::instance().getMemMngr().setAlt(val, deref);
	return val;
}

void toPri(ExpVal & val) {
	Compiler & comp = Compiler::instance();
	auto & mngr = comp.getMemMngr();
	
	if(mngr.isInPri(val, val.isReference()))
		mngr.clrPri();
	else if(mngr.isInAlt(val, val.isReference())) {
		mngr.storePri();
		comp.emit(Opcode::op_move_pri);
	}
	else if(mngr.isInPri(val, false)) {
		mngr.storePri();
		comp.emit(Opcode::op_load_i);
	}
	else if(mngr.isInAlt(val, false)) {
		mngr.storePri();
		comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_load_i);
	}
	else {
		mngr.storePri();
		val.toPri(true);
	}
}

void toAlt(ExpVal & val) {
	Compiler & comp = Compiler::instance();
	auto & mngr = comp.getMemMngr();
	
	if(mngr.isInAlt(val, val.isReference()))
		mngr.clrAlt();
	else if(mngr.isInPri(val, val.isReference())) {
		mngr.storeAlt();
		comp.emit(Opcode::op_move_alt);
	}
	else if(mngr.isInAlt(val, false)) {
		mngr.storeAlt();
		mngr.storePri();
		comp.emit(Opcode::op_move_pri);
		comp.emit(Opcode::op_load_i);
		comp.emit(Opcode::op_move_alt);
	}
	else if(mngr.isInPri(val, false)) {
		mngr.storeAlt();
		comp.emit(Opcode::op_load_i);
		comp.emit(Opcode::op_move_alt);
	}
	else {
		mngr.storeAlt();
		val.toAlt(true);
	}
}

void ExpVal::ExpValData::store(bool pri) {
	Compiler & comp = Compiler::instance();
	loc_ = ExpValLoc::Stack;
	
	if(pri)
		comp.emit(Opcode::op_push_pri);
	else
		comp.emit(Opcode::op_push_alt);
}

void ExpVal::keep(bool keep) {
	if(data_->loc_ == ExpValLoc::None) {
		auto & mngr = Compiler::instance().getMemMngr();
		if(mngr.isInPri(*this, false))
			mngr.keepPri(keep);
		else if(mngr.isInAlt(*this, false))
			mngr.keepAlt(keep);
	}
}

bool ExpVal::isInPri(bool deref) const {
	return Compiler::instance().getMemMngr().isInPri(*this, deref && isReference());
}

bool ExpVal::isInAlt(bool deref) const {
	return Compiler::instance().getMemMngr().isInAlt(*this, deref && isReference());
}

void ExpVal::toPri(bool deref) {
	Compiler & comp = Compiler::instance();
	if(type_.isReference() && deref) {
		switch(data_->loc_) {
			case ExpValLoc::Constexpr:
				comp.emit(Opcode::op_load_pri, data_->value_);
				break;
			case ExpValLoc::LocalVar:
				comp.emit(Opcode::op_lref_s_pri, data_->value_);
				break;
			case ExpValLoc::GlobVar:
				comp.emit(Opcode::op_lref_pri, data_->value_);
				break;
			case ExpValLoc::Stack:
				comp.emit(Opcode::op_pop_pri);
				comp.emit(Opcode::op_load_i);
				data_->loc_ = ExpValLoc::None;
				break;
			default:
				throw std::runtime_error("Can't load to pri.");
		}
	}
	else {
		switch(data_->loc_) {
			case ExpValLoc::Constexpr:
				if(data_->value_ == 0)
					comp.emit(Opcode::op_zero_pri);
				else
					comp.emit(Opcode::op_const_pri, data_->value_);
				break;
			case ExpValLoc::LocalVar:
				comp.emit(Opcode::op_load_s_pri, data_->value_);
				break;
			case ExpValLoc::GlobVar:
				comp.emit(Opcode::op_load_pri, data_->value_);
				break;
			case ExpValLoc::Stack:
				comp.emit(Opcode::op_pop_pri);
				data_->loc_ = ExpValLoc::None;
				break;
			default:
				throw std::runtime_error("Can't load to pri.");
		}
	}
}

void ExpVal::toAlt(bool deref) {
	Compiler & comp = Compiler::instance();
	
	if(type_.isReference() && deref) {
		switch(data_->loc_) {
			case ExpValLoc::Constexpr:
				comp.emit(Opcode::op_load_alt, data_->value_);
				break;
			case ExpValLoc::LocalVar:
				comp.emit(Opcode::op_lref_s_alt, data_->value_);
				break;
			case ExpValLoc::GlobVar:
				comp.emit(Opcode::op_lref_alt, data_->value_);
				break;
			case ExpValLoc::Stack:
				comp.emit(Opcode::op_xchg);
				comp.emit(Opcode::op_pop_pri);
				comp.emit(Opcode::op_load_i);
				comp.emit(Opcode::op_xchg);
				data_->loc_ = ExpValLoc::None;
				break;
			default:
				throw std::runtime_error("Can't load to alt.");
		}

	}
	else {
		switch(data_->loc_) {
			case ExpValLoc::Constexpr:
				if(data_->value_ == 0)
					comp.emit(Opcode::op_zero_alt);
				else
					comp.emit(Opcode::op_const_alt, data_->value_);
				break;
			case ExpValLoc::LocalVar:
				comp.emit(Opcode::op_load_s_alt, data_->value_);
				break;
			case ExpValLoc::GlobVar:
				comp.emit(Opcode::op_load_alt, data_->value_);
				break;
			case ExpValLoc::Stack:
				comp.emit(Opcode::op_pop_alt);
				data_->loc_ = ExpValLoc::None;
				return;
			default:
				throw std::runtime_error("Can't load to alt.");
		}
	}	
}

void ExpVal::push(bool opt) {
	Compiler & comp = Compiler::instance();
	
	if(isOnStack())
		return;
	if(isInPri()) {
		if(opt)
			comp.emitOp(Opcode::op_push_pri);
		else
			comp.emit(Opcode::op_push_pri);
	}
	else if(isInAlt()) {
		if(opt)
			comp.emitOp(Opcode::op_push_alt);
		else
			comp.emit(Opcode::op_push_alt);
	}	
	else if(type_.isReference()) {
		::toPri(*this);
		comp.emit(Opcode::op_push_pri);
	}
	else if(isConstexpr())
		comp.emit(Opcode::op_push_c, data_->value_);
	else if(data_->loc_ == ExpValLoc::LocalVar)
		comp.emit(Opcode::op_push_s, data_->value_);
	else if(data_->loc_ == ExpValLoc::GlobVar)
		comp.emit(Opcode::op_push, data_->value_);
}

ExpVal ExpVal::getPtr() {
	Type type = type_;
	if(type.getPtrLevel() != 0)
		type.removeFlag(VariableFlags::Const);
	else if(type.isConst())
		type.removeFlag(VariableFlags::Const).addFlag(VariableFlags::ConstPtr);
	type.setPtrLevel(type.getPtrLevel() + 1);
	
	if(type.isReference()) {
		type.removeFlag(VariableFlags::Reference);
		return ExpVal(type, *this);
	}
	
	switch(data_->loc_) {
		case ExpValLoc::GlobVar:
			return ExpVal(type, ExpValLoc::Constexpr, data_->value_);
		case ExpValLoc::LocalVar:
		case ExpValLoc::FuncArg: {
			Compiler& comp = Compiler::instance();
			comp.getMemMngr().storePri();
			comp.emit(Opcode::op_addr_pri, data_->value_);
			ExpVal val = ExpVal(type);
			comp.getMemMngr().setPri(val, false);
			return val;
		}
		default:
			throw std::runtime_error("Can't get pointer of this object.");
	}
}

ExpVal ExpVal::deref() {
	Type type = type_;
	if(type.getPtrLevel() == 0)
		throw std::runtime_error("Dereferencing non-pointer.");
	
	if(type.getPtrLevel() != 1)
		type.removeFlag(VariableFlags::Constexpr);
	else if(type.isConstPtr())
		type.removeFlag(VariableFlags::Constexpr | VariableFlags::ConstPtr).addFlag(VariableFlags::Const);	
	else 
		type.removeFlag(VariableFlags::Constexpr | VariableFlags::Const | VariableFlags::ConstPtr);	
	type.setPtrLevel(type.getPtrLevel() - 1);
	
	Compiler& comp = Compiler::instance();
	auto & mngr = comp.getMemMngr();
	
	if(!type_.isReference()) {
		type.addFlag(VariableFlags::Reference);
		return ExpVal(type, *this);
	}
	if(isConstexpr()) {
		comp.getMemMngr().storePri();
		comp.emit(Opcode::op_load_pri, data_->value_);
	}
	else {
		::toPri(*this);
		comp.emit(Opcode::op_load_i);
	}
	
	ExpVal val = ExpVal(type);
	mngr.setPri(val, false);
	return val;
}

ExpVal::~ExpVal() noexcept(false) {
	if(data_ && data_->refs_ == 1) {
		if(data_->loc_ == ExpValLoc::Stack)
			Compiler::instance().emit(Opcode::op_stack, 4);
		
		auto & mngr = Compiler::instance().getMemMngr();
		if(mngr.isInPri(*this, false) || mngr.isInPri(*this, true))
			mngr.clrPri();
		if(mngr.isInAlt(*this, false) || mngr.isInAlt(*this, true))
			mngr.clrAlt();
		delete data_;
	}
}

