
#pragma once

#include <string>
#include <vector>

#include "types.hpp"
#include "idents.hpp"
#include "exp_val.hpp"

struct LocalVariable {
	Type type;
	unsigned int layer;
	Identifiers::iterator it;
	int pos;
	
	LocalVariable(Type type, unsigned int layer, Identifiers::iterator it, int pos) :
		type(std::move(type)), layer(layer), it(std::move(it)), pos(pos) {}
};

class LocalVariables {
	int stk_ = 0;
	unsigned int layer_ = 0;
	
	std::vector<int> oldStk_;
	std::vector<LocalVariable> localVars_;
public:
	unsigned int getLayer() const { return layer_; }
	void pushLayer() { ++layer_; oldStk_.push_back(stk_); }
	void popLayer(bool gen = true);
	
	void freeStack();
	LocalVariable& operator[](unsigned int id) { return localVars_[id]; }
	const LocalVariable& operator[](unsigned int id) const { return localVars_[id]; }
	
	int addLocalVariable(std::string name, const Type& type, ExpVal & val);
	int addLocalArray(std::string name, const Type& type, unsigned int count);
};







