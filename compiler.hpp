
#pragma once

#include <iostream>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <list>

#include <cassert>

#include "opcodes.hpp"
#include "options.hpp"
#include "types.hpp"
#include "types_conv.hpp"
#include "types_ops.hpp"
#include "code.hpp"
#include "idents.hpp"
#include "local_vars.hpp"
#include "literals.hpp"

using uint = unsigned int;
using cell = Opcodes::cell;
static_assert(sizeof(uint) == 4, "Need 4 byte data.");

struct NatFunc {
	unsigned int floatadd = 0;
	unsigned int floatsub = 0;
	unsigned int floatmul = 0;
	unsigned int floatdiv = 0;
	unsigned int floatcmp = 0;
	unsigned int floatctr = 0;
	unsigned int floatround = 0;
};

enum class LexemType {
	EndOfFile,
	Line,
	Int,
	Hex,
	Float,
	String,
	Identifier,
	Comma,
	Semicolon,
	Const,
	Public,
	Native,
	Return,
	Assign,
	LeftBracket,
	RightBracket,
	LeftCurly,
	RightCurly,
	LeftSquare,
	RightSquare,
	Operator,
	UnOperator,
	If, 
	Else,
	While,
	Do,
	For,
	Break,
	Continue,
	Constexpr,
	Explicit,
	OperatorFunc,
	Include,
	PragmaDynamic,
	True,
	False,
	Dots,
	Typedef,
	OperatorKey
};

struct GlobalVariable {
	std::string name;
	ExpVal val;
	uint pos;
	
	GlobalVariable(std::string name, ExpVal val, uint pos) : name(std::move(name)), val(std::move(val)), pos(pos) {}
	
	const Type & type() const { return val.getType(); }
	ExpVal value() const { return val; }
};

struct Argument {
	std::string name;
	uint pos;
	Type type;
	
	Argument(std::string name, uint pos, Type type) : name(std::move(name)), pos(pos), type(type) {}
};

struct Function {
	std::string name;
	Type type;
	uint pos;
	std::vector<Argument> args;
	
	Function(std::string name, Type ret, uint p) : name(std::move(name)), type(ret), pos(p) {}
};

extern void setFile(FILE* file, bool shouldPush);
extern int yylex();

class Compiler {
	Options options_;
	TypeConversions convs_;
	Operators opers_;
	Code code_;
	std::vector<Code> codeStack_;
	Literals literals_;
	
	Identifiers idents_;
	LocalVariables localVars_;
	
	std::vector<TypeInfo> types_;
	std::vector<GlobalVariable> globVars_;
	std::vector<Function> funcs_;
	std::vector<std::pair<Argument*, Identifiers::iterator>> funcArgs_;
	
	uint line_ = 1;
	
	LexemType yytype_;
	std::string yytext_;
	
	std::vector<std::tuple<FILE*, std::string, uint>> files_;
	
	Compiler() noexcept;
	Compiler(const Compiler &) = delete;
	Compiler(Compiler &&) = delete;
	Compiler& operator=(const Compiler&) = delete;
	Compiler& operator=(Compiler&&) = delete;
public:
	static Compiler & instance() {
		static Compiler comp;
		return comp;
	}
	
	Options & getOptions() { return options_; }
	const Options & getOptions() const { return options_; }
	
	Literals& getLiterals() { return literals_; }
	const Literals& getLiterals() const { return literals_; }
	
	TypeConversions& getConvs() { return convs_; }
	const TypeConversions& getConvs() const { return convs_; }
	
	Operators& getOperators() { return opers_; }
	const Operators& getOperators() const { return opers_; }
	
	NatFunc natFunc_;
	
	void pushInput(std::string input) {
		FILE* file = fopen(input.c_str(), "r");
		if(!file)
			throw std::runtime_error("Can't open file '" + std::string(input) + "'.");
		
		setFile(file, true);
		files_.push_back(std::make_tuple(file, std::move(input), line_));
		line_ = 1;
	}
	bool popInput() {
		line_ = std::get<2>(files_.back());
		files_.pop_back();
		if(files_.empty())
			return false;

		setFile(std::get<0>(files_.back()), false);
		return true;
	}
	
	uint addType(std::string name, uint base, bool imp = true) {
		uint id = types_.size();
		types_.push_back(TypeInfo{name, 4});
		idents_.addGlobIdent(std::move(name), IdentifierType::Type, id);
		
		if(base != 0) {
			convs_.addConversionNop(id, base, imp);
			convs_.addConversionNop(base, id, imp);
		}
		return id;
	}
	
	Code& code() { return codeStack_.empty() ? code_ : codeStack_.back(); }
	const Code& code() const { return codeStack_.empty() ? code_ : codeStack_.back(); }
	
	void newCode(Code c = Code()) { codeStack_.push_back(std::move(c)); }
	Code releaseCode() { Code ret = std::move(codeStack_.back()); codeStack_.pop_back(); return ret; }
	
	uint nextInst() const { return code().nextInst(); }
	uint makeLabel() { return code().makeLabel(); }
	//void fillLabel(uint id, uint pos) { return code().fillLabel(id, pos); }
	void fillLabelNext(uint id) { return code().fillLabelNext(id); }
	
	uint emit(Opcode op) { return code().emit(op); }
	uint emit(Opcode op, cell arg1) { return code().emit(op, arg1); }
	uint emit(Opcode op, cell arg1, cell arg2) { return code().emit(op, arg1, arg2); }
	
	void emitOp(Opcode op) { code().emitOp(op); }
	void emitOp(Opcode op, cell arg1) { code().emitOp(op, arg1); }
	
	ExpValManager & getMemMngr() { return code().getMemMngr(); }
	
	Identifiers& getIdentifiers() { return idents_; }
	const Identifiers& getIdentifiers() const { return idents_; }
	
	void pushLayer() { localVars_.pushLayer(); }
	void popLayer(bool gen = true) { localVars_.popLayer(gen); }
	void freeStack() { localVars_.freeStack(); }
	
	void addGlobalVariable(std::string name, const Type & type, uint val = 0) {
		if(type == BasicType::Void)
			throw std::runtime_error("Global variable '" + name +"' can't have void type.");
		
		uint id = globVars_.size();
		uint pos = type.isConstexpr() ? 0 : literals_.add(val);
		
		if(type.isConstexpr())
			globVars_.emplace_back(name, ExpVal(type, ExpValLoc::Constexpr, val), pos);
		else
			globVars_.emplace_back(name, ExpVal(type, ExpValLoc::GlobVar, pos), pos);
		
		idents_.addGlobIdent(std::move(name), IdentifierType::GlobVar, id);
	}
	void addGlobalArray(std::string name, Type type, uint count) {
		uint size = getTypeInfo(type).size * count;
		Literal lit;
		lit.resize((size + 3) / 4, 0u);
		uint pos = literals_.add(std::move(lit));
		type.setPtrLevel(type.getPtrLevel() + 1);
		type.addFlag(VariableFlags::Constexpr | VariableFlags::Const);
		addGlobalVariable(std::move(name), type, pos);
	}
	int addLocalVariable(std::string name, const Type & type, ExpVal & val) {
		return localVars_.addLocalVariable(std::move(name), type, val);
	}
	int addLocalArray(std::string name, const Type & type, uint count) {
		return localVars_.addLocalArray(std::move(name), type, count);
	}
	
	Identifier getIdent(const std::string & name) const { return idents_.getIdent(name); }
	const LocalVariable& getLocalVariable(uint id) const { return localVars_[id]; }
	const GlobalVariable& getGlobalVariable(uint id) const { return globVars_[id]; }
	
	void addFuncArg(Argument* arg) {
		if(arg->type == BasicType::Void)
			throw std::runtime_error("Argument '" + arg->name +"' can't have void type.");
		
		auto it = idents_.addLocalIdent(arg->name, IdentifierType::FuncArg, funcArgs_.size());
		funcArgs_.push_back(std::make_pair(arg, it));
	}
	void clearFuncArgs() {
		for(const auto & i : funcArgs_) {
			i.second->second.pop_back();
			if(i.second->second.empty())
				idents_.erase(i.second);
		}
		funcArgs_.clear();
	}
	const Argument & getFuncArg(uint id) const {
		return *funcArgs_[id].first;
	}
	
	uint addFunc(std::string name, Type type) {
		uint id = funcs_.size();
		funcs_.emplace_back(name, type, 0);
		idents_.addGlobIdent(std::move(name), IdentifierType::Func, id);
		return id;
	}
	Function& getFunc(uint id) { return funcs_[id]; }
	const Function& getFunc(uint id) const { return funcs_[id]; }
	Function & lastFunc() { return funcs_.back(); }
	
	void token(LexemType t, const char* str) {
		yytype_ = t;
		yytext_ = str;
	}
	void parse();
	std::pair<LexemType, std::string> getToken() {
		auto ret = std::make_pair(yytype_, yytext_);
		while(true) {
			if(!yylex()) {
				yytype_ = LexemType::EndOfFile;
				yytext_ = "end-of-file";
				return ret;
			}
			if(yytype_ != LexemType::Line)
				return ret;
			++line_;
		}
	}
	std::pair<LexemType, std::string> peekToken() const {
		return std::make_pair(yytype_, yytext_);
	}
	
	const TypeInfo & getTypeInfo(uint id) const {
		return types_[id];
	}
	const TypeInfo & getTypeInfo(Type t) const {
		return types_[t.getID()];
	}
	
	void warn(Warning id, std::string msg) {
		if(options_.isWarningEnabled(id))
			std::cerr << "Warning on line " << line_ << ": " << msg << std::endl;
	}
	
	void write(std::ostream & os);
	
	void debug_dump() const {
		std::cout << "\n";
		idents_.debug_dump();
		code_.debug_dump();
	}
};





