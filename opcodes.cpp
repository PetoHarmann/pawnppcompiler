

#include "opcodes.hpp"
#include <unordered_map>







const Opcodes::OPCODE Opcodes::opcodelist[] = {
	{ 0, "???",        0 },
	{ 1, "load.pri",   1, ParamType::GlobVar },
	{ 2, "load.alt",   1, ParamType::GlobVar },
	{ 3, "load.s.pri", 1 },
	{ 4, "load.s.alt", 1 },
	{ 5, "lref.pri",   1, ParamType::GlobVar },
	{ 6, "lref.alt",   1, ParamType::GlobVar },
	{ 7, "lref.s.pri", 1 },
	{ 8, "lref.s.alt", 1 },
	{ 9, "load.i",     0 },
	{ 10, "lodb.i",     1 },
	{ 11, "const.pri",  1 },
	{ 12, "const.alt",  1 },
	{ 13, "addr.pri",   1 },
	{ 14, "addr.alt",   1 },
	{ 15, "stor.pri",   1, ParamType::GlobVar },
	{ 16, "stor.alt",   1, ParamType::GlobVar },
	{ 17, "stor.s.pri", 1 },
	{ 18, "stor.s.alt", 1 },
	{ 19, "sref.pri",   1, ParamType::GlobVar },
	{ 20, "sref.alt",   1, ParamType::GlobVar },
	{ 21, "sref.s.pri", 1 },
	{ 22, "sref.s.alt", 1 },
	{ 23, "stor.i",     0 },
	{ 24, "strb.i",     1 },
	{ 25, "lidx",       0 },
	{ 26, "lidx.b",     1 },
	{ 27, "idxaddr",    0 },
	{ 28, "idxaddr.b",  1 },
	{ 29, "align.pri",  1 },
	{ 30, "align.alt",  1 },
	{ 31, "lctrl",      1 },
	{ 32, "sctrl",      1 },
	{ 33, "move.pri",   0 },
	{ 34, "move.alt",   0 },
	{ 35, "xchg",       0 },
	{ 36, "push.pri",   0 },
	{ 37, "push.alt",   0 },
	{ 38, "push.r",     1 },  /* obsolete (never generated) */
	{ 39, "push.c",     1 },
	{ 40, "push",       1, ParamType::GlobVar },
	{ 41, "push.s",     1 },
	{ 42, "pop.pri",    0 },
	{ 43, "pop.alt",    0 },
	{ 44, "stack",      1 },
	{ 45, "heap",       1 },
	{ 46, "proc",       0 },
	{ 47, "ret",        0 },
	{ 48, "retn",       0 },
	{ 49, "call",       1, ParamType::Function },
	{ 50, "call.pri",   0 },
	{ 51, "jump",       1, ParamType::Label },
	{ 52, "jrel",       1 },  /* same as jump, since version 10 */
	{ 53, "jzer",       1, ParamType::Label },
	{ 54, "jnz",        1, ParamType::Label },
	{ 55, "jeq",        1, ParamType::Label },
	{ 56, "jneq",       1, ParamType::Label },
	{ 57, "jless",      1, ParamType::Label },
	{ 58, "jleq",       1, ParamType::Label },
	{ 59, "jgrtr",      1, ParamType::Label },
	{ 60, "jgeq",       1, ParamType::Label },
	{ 61, "jsless",     1, ParamType::Label },
	{ 62, "jsleq",      1, ParamType::Label },
	{ 63, "jsgrtr",     1, ParamType::Label },
	{ 64, "jsgeq",      1, ParamType::Label },
	{ 65, "shl",        0 },
	{ 66, "shr",        0 },
	{ 67, "sshr",       0 },
	{ 68, "shl.c.pri",  1 },
	{ 69, "shl.c.alt",  1 },
	{ 70, "shr.c.pri",  1 },
	{ 71, "shr.c.alt",  1 },
	{ 72, "smul",       0 },
	{ 73, "sdiv",       0 },
	{ 74, "sdiv.alt",   0 },
	{ 75, "umul",       0 },
	{ 76, "udiv",       0 },
	{ 77, "udiv.alt",   0 },
	{ 78, "add",        0 },
	{ 79, "sub",        0 },
	{ 80, "sub.alt",    0 },
	{ 81, "and",        0 },
	{ 82, "or",         0 },
	{ 83, "xor",        0 },
	{ 84, "not",        0 },
	{ 85, "neg",        0 },
	{ 86, "invert",     0 },
	{ 87, "add.c",      1 },
	{ 88, "smul.c",     1 },
	{ 89, "zero.pri",   0 },
	{ 90, "zero.alt",   0 },
	{ 91, "zero",       1, ParamType::GlobVar },
	{ 92, "zero.s",     1 },
	{ 93, "sign.pri",   0 },
	{ 94, "sign.alt",   0 },
	{ 95, "eq",         0 },
	{ 96, "neq",        0 },
	{ 97, "less",       0 },
	{ 98, "leq",        0 },
	{ 99, "grtr",       0 },
	{ 100, "geq",        0 },
	{ 101, "sless",      0 },
	{ 102, "sleq",       0 },
	{ 103, "sgrtr",      0 },
	{ 104, "sgeq",       0 },
	{ 105, "eq.c.pri",   1 },
	{ 106, "eq.c.alt",   1 },
	{ 107, "inc.pri",    0 },
	{ 108, "inc.alt",    0 },
	{ 109, "inc",        1, ParamType::GlobVar },
	{ 110, "inc.s",      1 },
	{ 111, "inc.i",      0 },
	{ 112, "dec.pri",    0 },
	{ 113, "dec.alt",    0 },
	{ 114, "dec",        1, ParamType::GlobVar },
	{ 115, "dec.s",      1 },
	{ 116, "dec.i",      0 },
	{ 117, "movs",       1 },
	{ 118, "cmps",       1 },
	{ 119, "fill",       1 },
	{ 120, "halt",       1 },
	{ 121, "bounds",     1 },
	{ 122, "sysreq.pri", 0 },
	{ 123, "sysreq.c",   1, ParamType::Native },
	{ 124, "file",       1 },
	{ 125, "line",       2 },
	{ 126, "symbol",     1 },
	{ 127, "srange",     2 },        /* version 1 */
	{ 128, "jump.pri",   0 },        /* version 1 */
	{ 129, "switch",     1 },    /* version 1 */
	{ 130, "casetbl",    2 },      /* version 1 */
	{ 131, "swap.pri",   0 },        /* version 4 */
	{ 132, "swap.alt",   0 },        /* version 4 */
	{ 133, "push.adr",   1 },        /* version 4 */
	{ 134, "nop",        0 },        /* version 6 */
	{ 135, "sysreq.n",   2 },        /* version 9 (replaces SYSREQ.d from earlier version) */
	{ 136, "symtag",     1 },        /* version 7 */
	{ 137, "break",      0 },        /* version 8 */
	{ 138, "push2.c",    2 },        /* version 9 */
	{ 139, "push2",      2 },        /* version 9 */
	{ 140, "push2.s",    2 },        /* version 9 */
	{ 141, "push2.adr",  2 },        /* version 9 */
	{ 142, "push3.c",    3 },        /* version 9 */
	{ 143, "push3",      3 },        /* version 9 */
	{ 144, "push3.s",    3 },        /* version 9 */
	{ 145, "push3.adr",  3 },        /* version 9 */
	{ 146, "push4.c",    4 },        /* version 9 */
	{ 147, "push4",      4 },        /* version 9 */
	{ 148, "push4.s",    4 },        /* version 9 */
	{ 149, "push4.adr",  4 },        /* version 9 */
	{ 150, "push5.c",    5 },        /* version 9 */
	{ 151, "push5",      5 },        /* version 9 */
	{ 152, "push5.s",    5 },        /* version 9 */
	{ 153, "push5.adr",  5 },        /* version 9 */
	{ 154, "load.both",  2 },        /* version 9 */
	{ 155, "load.s.both",2 },        /* version 9 */
	{ 156, "const",      2 },        /* version 9 */
	{ 157, "const.s",    2 },        /* version 9 */
};

class Translator {
	std::unordered_map<std::string, int> map;
public:
	Translator();
	int find(const std::string & op);
};

Translator::Translator() {
	for (int i = 0; i < (sizeof(Opcodes::opcodelist)/sizeof(Opcodes::OPCODE)); ++i) {
		map.insert(std::make_pair(std::string(Opcodes::opcodelist[i].name), Opcodes::opcodelist[i].opcode));
	}
}

int Translator::find(const std::string & op) {
	auto it = map.find(op);
	if (it != map.end()) return it->second;
	return 0;
}

int Opcodes::getOpcode(const std::string & op) {
	static Translator tr;
	return tr.find(op);
}
