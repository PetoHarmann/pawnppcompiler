
#pragma once

#include <bitset>

enum class Warning : unsigned char {
	None,
	ShadowsVariable,
	ConstexprIf,
	Unreachable,
	ShouldReturn,
	UnnecessaryCast
};

class Options {
	std::bitset<256> warnDisabled_;
	unsigned int stackSize_ = 0x4000;
public:
	bool isWarningEnabled(Warning id) const {
		return !warnDisabled_[static_cast<std::size_t>(id)];
	}
	void setWarning(Warning id, bool set) {
		warnDisabled_[static_cast<std::size_t>(id)] = !set;
	}
	unsigned int getStackSize() const {
		return stackSize_;
	}
	void setStackSize(unsigned int size) {
		stackSize_ = size;
	}
};



