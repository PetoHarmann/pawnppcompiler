%{
	#include <iostream>
	#include <fstream>
	#include "compiler.hpp"
	using namespace std;
	extern int yylex();
	Compiler & comp = Compiler::instance();
%}
%option noyywrap
%%
[ \t\r]	         					;
\n									{ comp.token(LexemType::Line, yytext); return 1; }
\/\/[^\n]*\n						{ comp.token(LexemType::Line, yytext); return 1; }
true								{ comp.token(LexemType::True, yytext); return 1; }
false								{ comp.token(LexemType::False, yytext); return 1; }
constexpr							{ comp.token(LexemType::Constexpr, yytext); return 1; }
const 								{ comp.token(LexemType::Const, yytext); return 1; }
public								{ comp.token(LexemType::Public, yytext); return 1; }
native								{ comp.token(LexemType::Native, yytext); return 1; }
explicit							{ comp.token(LexemType::Explicit, yytext); return 1; }
operator							{ comp.token(LexemType::OperatorFunc, yytext); return 1; }
return 								{ comp.token(LexemType::Return, yytext); return 1; }
if 									{ comp.token(LexemType::If, yytext); return 1; }
else 								{ comp.token(LexemType::Else, yytext); return 1; }
while								{ comp.token(LexemType::While, yytext); return 1; }
do 									{ comp.token(LexemType::Do, yytext); return 1; }
for									{ comp.token(LexemType::For, yytext); return 1; }
break								{ comp.token(LexemType::Break, yytext); return 1; }
continue							{ comp.token(LexemType::Continue, yytext); return 1; }
typedef								{ comp.token(LexemType::Typedef, yytext); return 1; }
#include							{ comp.token(LexemType::Include, yytext); return 1; }
#pragma[ \t]+dynamic				{ comp.token(LexemType::PragmaDynamic, yytext); return 1; }
\.\.\.								{ comp.token(LexemType::Dots, yytext); return 1; }
\+\+								{ comp.token(LexemType::UnOperator, "0"); return 1; }
\-\-								{ comp.token(LexemType::UnOperator, "1"); return 1; }
; 									{ comp.token(LexemType::Semicolon, yytext); return 1; }
, 									{ comp.token(LexemType::Comma, yytext); return 1; }
== 									{ comp.token(LexemType::Operator, "="); return 1; }
!= 									{ comp.token(LexemType::Operator, "0"); return 1; }
\<=									{ comp.token(LexemType::Operator, "1"); return 1; }
\>=									{ comp.token(LexemType::Operator, "2"); return 1; }
\|\| 								{ comp.token(LexemType::Operator, "3"); return 1; }
&& 									{ comp.token(LexemType::Operator, "4"); return 1; }
\| 									{ comp.token(LexemType::Operator, "5"); return 1; }
& 									{ comp.token(LexemType::Operator, "6"); return 1; }
\<\<								{ comp.token(LexemType::Operator, "7"); return 1; }
\>\>\> 								{ comp.token(LexemType::Operator, "9"); return 1; }
\>\> 								{ comp.token(LexemType::Operator, "8"); return 1; }
\^ 									{ comp.token(LexemType::Operator, "\""); return 1; }
[<>] 								{ comp.token(LexemType::Operator, yytext); return 1; }
= 									{ comp.token(LexemType::Assign, yytext); return 1; }
\-									{ comp.token(LexemType::Operator, yytext); return 1; }
[\+\*\/%]							{ comp.token(LexemType::Operator, yytext); return 1; }
[!~]								{ comp.token(LexemType::UnOperator, yytext); return 1; }
\( 									{ comp.token(LexemType::LeftBracket, yytext); return 1; }
\) 									{ comp.token(LexemType::RightBracket, yytext); return 1; }
\{ 									{ comp.token(LexemType::LeftCurly, yytext); return 1; }
\} 									{ comp.token(LexemType::RightCurly, yytext); return 1; }
\[ 									{ comp.token(LexemType::LeftSquare, yytext); return 1; }
\] 									{ comp.token(LexemType::RightSquare, yytext); return 1; }
(0|([1-9][0-9]*))\.[0-9]+  			{ comp.token(LexemType::Float, yytext); return 1; }
0x[0-9a-fA-F]+						{ comp.token(LexemType::Int, yytext); return 1; }
[0-9]+          					{ comp.token(LexemType::Int, yytext); return 1; }
\"[^\\"\n]*(\\[^\n][^\\"\n]*)*\"    { comp.token(LexemType::String, yytext); return 1; }
[a-zA-Z0-9_]+   					{ comp.token(LexemType::Identifier, yytext); return 1; }
%%
void setFile(FILE* file, bool shouldPush) {
	yyin = file;
	if(shouldPush) 
		yypush_buffer_state(yy_create_buffer( yyin, YY_BUF_SIZE ));
	else
		yypop_buffer_state();
}
	
int main(int argc, char** argv) {
	if(argc < 2) {
		cout << argv[0] << " in_file" << endl;
		return -1;
	}
	
	try {
		comp.pushInput(argv[1]);
		comp.parse();
		//comp.debug_dump();
		
		std::ofstream os("out.amx", std::ofstream::out | std::ofstream::binary);
		comp.write(os);
	}
	catch(const exception & e) {
		//comp.debug_dump();
		cerr << e.what() << endl;
		return -1;
	}
	return 0;
}
