

#include "types.hpp"
#include "compiler.hpp"

using namespace std;

ostream & operator<<(ostream & os, const Type & t) {
		if(t.isPublic())
			os << "public ";
		else if(t.isNative())
			os << "native ";
		
		if(t.getPtrLevel()) {
			if(t.isConstPtr())
				os << "const";
		}
		else {
			if(t.isConst())
				os << "const ";
			else if(t.isConstexpr())
				os << "constexpr ";
		}
		for(int i = 0, n = t.getPtrLevel(); i < n; ++i)
			os << "*";
		
		if(t.getPtrLevel()) {
			if(t.isConst())
				os << "const ";
			else if(t.isConstexpr())
				os << "constexpr ";
		}
		
		os << Compiler::instance().getTypeInfo(t).name;
		
		if(t.isVarArgs())
			os << "...";
		return os;
	}




