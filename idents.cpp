
#include "idents.hpp"
#include "compiler.hpp"

using namespace std;

Identifiers::iterator Identifiers::addLocalIdent(string name, IdentifierType type, unsigned int id) {
	auto it = idents_.find(name);
	if(it == idents_.end()) {
		list<Identifier> l;
		l.emplace_back(type, id);
		return idents_.emplace(move(name), move(l)).first;
	}

	Compiler& comp = Compiler::instance();
	if(it->second.back().type == IdentifierType::LocalVar) { 
		comp.warn(Warning::ShadowsVariable, "Variable '" + name + "' shadows variable at preceding level.");
	}
	else if(it->second.back().type != IdentifierType::GlobVar && it->second.back().type != IdentifierType::FuncArg)
		throw runtime_error("Symbol already defined '" + name + "'.");
	
	it->second.emplace_back(type, id);
	return it;
}














