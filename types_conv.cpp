
#include "types_ops.hpp"
#include "compiler.hpp"

using namespace std;

void FuncTypeConversion::gen() const {
	Compiler & comp = Compiler::instance();
	Function& func = comp.getFunc(funcID_);
	
	if(func.type.isNative()) {
		comp.emit(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 4u);
		comp.emit(Opcode::op_sysreq_c, func.pos);
		comp.emit(Opcode::op_stack, 8);
	}
	else {
		comp.emit(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 4u);
		comp.emit(Opcode::op_call, func.pos);
	}
}

optional<ExpVal> TypeConversions::tryConvert(const TypeConversion* conv, ExpVal& from, const Type& toType) const {
	if(!conv)
		return nullopt;

	if(conv->isNop()) {
		Type resType(toType);
		if(from.isConstexpr())
			resType.addFlag(VariableFlags::Constexpr);
		return ExpVal(resType, from);
	}
	
	if(from.isConstexpr() && conv->isConstexpr())
		return ExpVal(toType.getID(), conv->convert(from.getCell()));
	
	Compiler& comp = Compiler::instance();

	if(from.isInPri())
		comp.getMemMngr().clrPri();
	else {
		comp.getMemMngr().storePri();
		from.toPri();
	}
	conv->gen();
	return ExpVal::fromPri(toType, false);
}

class IntFloatConversion : public TypeConversion {
public:	
	static IntFloatConversion& instance() {
		static IntFloatConversion obj;
		return obj;
	}
	
	bool isConstexpr() const override { return true; }
	
	void gen() const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 4);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatctr);
		comp.emit(Opcode::op_stack, 8);
	}
	unsigned int convert(unsigned int value) const override { return ExpVal::toBin(static_cast<float>(static_cast<int>(value))); }
};

class FloatIntConversion : public TypeConversion {
public:	
	static FloatIntConversion& instance() {
		static FloatIntConversion obj;
		return obj;
	}
	
	bool isConstexpr() const override { return true; }
	
	void gen() const override {
		Compiler & comp = Compiler::instance();
		comp.emit(Opcode::op_push_c, 1);
		comp.emit(Opcode::op_push_pri);
		comp.emit(Opcode::op_push_c, 8);
		comp.emit(Opcode::op_sysreq_c, comp.natFunc_.floatround);
		comp.emit(Opcode::op_stack, 12);
	}
	unsigned int convert(unsigned int value) const override { return static_cast<int>(ExpVal::toFloat(value)); }
};

class IntBoolConversion : public TypeConversion {
public:	
	static IntBoolConversion& instance() {
		static IntBoolConversion obj;
		return obj;
	}
	
	bool isConstexpr() const override { return true; }
	
	void gen() const override {
		Compiler& comp = Compiler::instance();
		comp.emit(Opcode::op_neg);
		comp.emit(Opcode::op_neg);
	}
	unsigned int convert(unsigned int value) const override { return !!value; }
};

TypeConversions::TypeConversions() noexcept {
	using uint = unsigned int;
	
	addConversion(uint(BasicType::Int), uint(BasicType::Float), &IntFloatConversion::instance(), true);
	addConversion(uint(BasicType::Float), uint(BasicType::Int), &FloatIntConversion::instance(), false);
	
	addConversionNop(uint(BasicType::Bool), uint(BasicType::Int), false);
	addConversion(uint(BasicType::Int), uint(BasicType::Bool), &IntBoolConversion::instance(), false);
}
