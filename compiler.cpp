
#include "compiler.hpp"
#include "mat_ops.hpp"
#include "amx_file_info.hpp"

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <algorithm>

using namespace std;

enum Priority {
	pMin,
	pComma,
	pAssign,
	pLOr,
	pLAnd,
	pBOr,
	pXor,
	pBAnd,
	pEqual,
	pLess,
	pThreeway,
	pBShift,
	pAddSub,
	pMulDiv,
	pPrefix,
	pSuffix,
	pMax
};

constexpr Priority operator+(Priority a, int b) { return static_cast<Priority>(static_cast<int>(a) + b); }
constexpr Priority operator-(Priority a, int b) { return static_cast<Priority>(static_cast<int>(a) - b); }
								//		  0        1          2       3       4         5        6       7
constexpr Priority opPriority[32] = { 	pMax,    pPrefix,  pXor,    pMax,    pMax,   pMulDiv,  pBAnd,   pMax,
										pMax,    pMax,	   pMulDiv, pAddSub, pComma, pAddSub,  pMax,    pMulDiv,
										pEqual,  pLess,    pLess,   pLOr,    pLAnd,  pBOr,     pBAnd,   pBShift,
										pBShift, pBShift,  pMax,    pMax,    pLess,  pEqual,   pLess,   pMax
									};
								
								//		0      1      2      3      4      5      6      7
constexpr bool opUnarity[32] =  	{ 	false, false, false, false, false, false, false, false,
										false, false, true,  false, false, true,  false, false,
										false, false, false, false, false, false,  true,  false,
										false, false, false, false, false, false, false, false
									};

constexpr Priority getOpPri(char op) { return opPriority[op - 0x20]; }
constexpr bool isOpUnary(char op) { return opPriority[op - 0x20]; }

static_assert(getOpPri('!') == pPrefix, 	"Bad Priority array.");
static_assert(getOpPri('%') == pMulDiv, 	"Bad Priority array.");
static_assert(getOpPri('&') == pBAnd, 		"Bad Priority array.");
static_assert(getOpPri('*') == pMulDiv, 	"Bad Priority array.");
static_assert(getOpPri('+') == pAddSub, 	"Bad Priority array.");
static_assert(getOpPri(',') == pComma, 		"Bad Priority array.");
static_assert(getOpPri('/') == pMulDiv, 	"Bad Priority array.");
static_assert(getOpPri('<') == pLess, 		"Bad Priority array.");
static_assert(getOpPri('>') == pLess, 		"Bad Priority array.");
static_assert(getOpPri('=') == pEqual, 		"Bad Priority array.");
static_assert(getOpPri('0') == pEqual, 		"Bad Priority array.");
static_assert(getOpPri('1') == pLess, 		"Bad Priority array.");
static_assert(getOpPri('2') == pLess, 		"Bad Priority array.");
static_assert(getOpPri('3') == pLOr, 		"Bad Priority array.");
static_assert(getOpPri('4') == pLAnd, 		"Bad Priority array.");
static_assert(getOpPri('5') == pBOr, 		"Bad Priority array.");
static_assert(getOpPri('6') == pBAnd, 		"Bad Priority array.");
static_assert(getOpPri('7') == pBShift, 	"Bad Priority array.");
static_assert(getOpPri('8') == pBShift, 	"Bad Priority array.");
static_assert(getOpPri('9') == pBShift, 	"Bad Priority array.");

static_assert(isOpUnary('-'), 				"Operator should be unary.");
static_assert(isOpUnary('&'), 				"Operator should be unary.");
static_assert(isOpUnary('*'), 				"Operator should be unary.");

void readVar(Compiler & comp);
void readFuncArgs(Compiler & comp);
bool readBlock(Compiler & comp, Type & ret_type, uint brk = 0, uint cnt = 0);
ExpVal readExpression(Compiler & comp, Priority priority = pMin);
bool readStatement(Compiler & comp, Type & ret_type, uint brk = 0, uint cnt = 0);

void readLexem(Compiler & comp, LexemType et, const char * msg) {
	auto [t, str] = comp.getToken();
	if(t != et)
		throw runtime_error("Expected '" + string(msg) +"' but found '" + string(str) + "'.");
}

Type getType(Compiler & comp) {
	Type type;
	
	auto [t, str] = comp.getToken();
	while(t != LexemType::Identifier) {
		switch(t) {
			case LexemType::Const: {
				type.addFlag(VariableFlags::Const);
				break;
			}
			case LexemType::Constexpr: {
				type.addFlag(VariableFlags::Const);
				type.addFlag(VariableFlags::Constexpr);
				break;
			}
			case LexemType::Public: {
				if(type.isNative())
					throw runtime_error("Can't have public native function.");
				type.addFlag(VariableFlags::Public);
				break;
			}
			case LexemType::Native: {
				if(type.isPublic())
					throw runtime_error("Can't have public native function.");
				type.addFlag(VariableFlags::Native);
				break;
			}
			case LexemType::Operator: {
				if(str[0] != '6')
					throw runtime_error("Expected type name but found '" + str + "'.");
				auto [t2, str2] = comp.peekToken();
				if(t2 != LexemType::Identifier)
					throw runtime_error("Expected 'auto' but found '" + str2 + "'.");
				auto ident = comp.getIdent(str2);
				if(ident.type != IdentifierType::Type || ident.id != static_cast<uint>(BasicType::Auto))
					throw runtime_error("Expected 'auto' but found '" + str2 + "'.");
				type.addFlag(VariableFlags::AutoRef);
				break;
			}
			default:
				throw runtime_error("Expected type name but found '" + string(str) + "'.");
		}
		std::tie(t, str) = comp.getToken();
	}
	
	Identifier ident = comp.getIdent(str);
	if(ident.type != IdentifierType::Type)
		throw runtime_error("Expected type name but found '" + string(str) + "'.");
	
	type.setID(ident.id);
	
	std::tie(t, str) = comp.peekToken();
	while(t == LexemType::Operator && str[0] == '*') {
		comp.getToken();
		type.setPtrLevel(type.getPtrLevel() + 1);
		
		std::tie(t, str) = comp.peekToken();
	}
	
	if(type.getPtrLevel()) {
		if(type.isConstexpr())
			throw runtime_error("Can't have pointer to constexpr.");
		if(type.isConst())
			type.removeFlag(VariableFlags::Const).addFlag(VariableFlags::ConstPtr);
	}
	
	if(t == LexemType::Const) {
		comp.getToken();
		type.addFlag(VariableFlags::Const);
	}
	else if(t == LexemType::Constexpr) {
		comp.getToken();
		type.addFlag(VariableFlags::Const);
		type.addFlag(VariableFlags::Constexpr);
	}
	
	return type;
}

template<typename T>
T formatStringLiteral(const string & str) {
	T ret;
	ret.reserve(str.length());
	
	const char * ch = str.c_str();
	++ch;
	while(*ch) {
		switch(*ch) {
			case '\\':
				++ch;
				switch(*ch) {
					case '\\':
						ret += '\\';
						break;
					case 'n':
						ret += '\n';
						break;
					case 't':
						ret += '\t';
						break;
					case '"':
						ret += '"';
						break;
					default:
						throw runtime_error("Invalid literal constant.");
				}
				break;
			case '"':
				break;
			default:
				ret += *ch;
				break;
		}
		++ch;
	}
	return ret;
}

string readString(Compiler & comp) {
	auto [t, str] = comp.getToken();
	if(t != LexemType::String) 
		throw runtime_error("Expected string literal but found '" + str +"'.");
	
	return formatStringLiteral<string>(str);
}

string readStringPeek(Compiler & comp) {
	auto [t, str] = comp.peekToken();
	if(t != LexemType::String) 
		throw runtime_error("Expected string literal but found '" + str +"'.");
	
	return formatStringLiteral<string>(str);
}

ExpVal readStringLiteral(Compiler & comp, string & str) {
	auto lit = formatStringLiteral<Literal>(str);
	lit += 0u;
	lit.shrink_to_fit();
	uint pos = comp.getLiterals().add(std::move(lit));
	return ExpVal(Type(BasicType::Int, VariableFlags::Constexpr | VariableFlags::Const | VariableFlags::ConstPtr | VariableFlags{1}), ExpValLoc::Constexpr, pos);
}

ExpVal readStringLiteral(Compiler & comp) {
	auto [t, str] = comp.getToken();
	if(t != LexemType::String) 
		throw runtime_error("Expected string literal but found '" + str +"'.");
	
	return readStringLiteral(comp, str);
}

ExpVal getInitValue(Compiler & comp) {
	auto [t, str] = comp.peekToken();
	if(t == LexemType::Assign) {
		comp.getToken();
		return readExpression(comp, pComma);
	}
	return ExpVal(BasicType::Void, 0);
}

uint readFuncCallArgs(Compiler & comp, const Function & func) {
	auto [t, str] = comp.getToken();
	if(t != LexemType::LeftBracket)
		throw runtime_error("Expected '(' but found '" + str + "'.");
	
	if(func.args.size())
		t = LexemType::Comma;
	else
		std::tie(t, str) = comp.getToken();
	
	vector<Code> exps;
	uint num = 0;
	
	// Read standard args
	for(const auto & i : func.args) {
		if(i.type.isVarArgs())
			break;
		if(t != LexemType::Comma)
			throw runtime_error("Expected ',' but found '" + str + "'.");
		
		++num;
		comp.newCode();
		ExpVal val = readExpression(comp, pComma);
		if(!i.type.accepts(val.getType()))
			val = comp.getConvs().convert(val, Type(i.type.getID()));
			
		val.push(true);
		exps.push_back(comp.releaseCode());
		
		std::tie(t, str) = comp.getToken();
	}
	// Read VarArgs
	if(!func.args.empty() && func.args.back().type.isVarArgs()) {
		const Type& type = func.args.back().type;
		while(t == LexemType::Comma) {
			++num;
			comp.newCode();
			ExpVal val = readExpression(comp, pComma);
			if(type.isAutoRef()) {
				if(val.getType().getPtrLevel() <= type.getPtrLevel())
					val = val.getPtr();
			}
			else if(!type.acceptsAuto(val.getType()))
				val = comp.getConvs().convert(val, Type(type.getID()));
			val.push(true);
			exps.push_back(comp.releaseCode());
			
			std::tie(t, str) = comp.getToken();
		}
	}
	// Reverse code order
	for(auto it = exps.rbegin(); it != exps.rend(); ++it)
		comp.code() << *it;
	
	if(t != LexemType::RightBracket)
		throw runtime_error("Expected ')' but found '" + str + "'.");
	
	comp.emit(Opcode::op_push_c, num * 4);
	return num;
}

ExpVal readPostfix(Compiler & comp, ExpVal val) {
	auto [t, str] = comp.peekToken();
	switch(t) {
		case LexemType::LeftSquare: {
			comp.getToken();
			val.keep();
			ExpVal val2 = readExpression(comp);
			val.keep(false);
			readLexem(comp, LexemType::RightSquare, "]");
			return comp.getOperators().getBinary('+').eval(val2, val).deref();
		}
		default:
			return val;
	}
}

ExpVal readExpMem(Compiler & comp) {
	auto [t, str] = comp.getToken();
	switch(t) {
		case LexemType::Operator: {
			if(!isOpUnary(str[0]))
				throw runtime_error("Expected expression but found '" + str + "'.");
			
			ExpVal val = readExpMem(comp);
			if(str[0] == '6')
				return val.getPtr();
			if(str[0] == '*')
				return val.deref();
			return comp.getOperators().getUnary(str[0]).eval(val);
		}
		case LexemType::UnOperator: {
			ExpVal val = readExpMem(comp);
			return comp.getOperators().getUnary(str[0]).eval(val);
		}
		case LexemType::Int:
		case LexemType::Hex:
			return ExpVal(BasicType::Int, getBinFromStr(t, str));
		case LexemType::Float:
			return ExpVal(BasicType::Float, getBinFromStr(t, str));
		case LexemType::True:
			return ExpVal(BasicType::Bool, 1);
		case LexemType::False:
			return ExpVal(BasicType::Bool, 0);
		case LexemType::String:
			return readStringLiteral(comp, str);
		case LexemType::Identifier: {
			Identifier ident = comp.getIdent(str);
			switch(ident.type) {
				case IdentifierType::GlobVar: {
					const GlobalVariable & var = comp.getGlobalVariable(ident.id);
					return readPostfix(comp, var.value());
				}
				case IdentifierType::LocalVar: {
					const LocalVariable & var = comp.getLocalVariable(ident.id);
					//if(var.flags & VariableFlags::Constexpr)
					//	return ExpVal(var.type, var.init);
					return readPostfix(comp, ExpVal(var.type, ExpValLoc::LocalVar, var.pos));
				}
				case IdentifierType::Func: {
					const Function & func = comp.getFunc(ident.id);
					uint numargs = readFuncCallArgs(comp, func);
					
					comp.getMemMngr().storePri();
					if(func.type.isNative()) {
						comp.emit(Opcode::op_sysreq_c, ident.id);
						comp.emit(Opcode::op_stack, numargs * 4 + 4);
					}
					else
						comp.emit(Opcode::op_call, ident.id);
					
					ExpVal val(func.type);
					comp.getMemMngr().setPri(val, false);
					return readPostfix(comp, std::move(val));
				}
				case IdentifierType::FuncArg: {
					const Argument & arg = comp.getFuncArg(ident.id);
					
					return readPostfix(comp, ExpVal(arg.type, ExpValLoc::LocalVar, arg.pos));
				}
				case IdentifierType::Type: {
					auto [t, str] = comp.getToken();
					if(t == LexemType::Operator && str[0] == '6') {
						readLexem(comp, LexemType::LeftBracket, "(");
						ExpVal val(Type(ident.id), readExpression(comp, pComma));
						readLexem(comp, LexemType::RightBracket, ")");
						return readPostfix(comp, std::move(val));
					}
					else if(t != LexemType::LeftBracket)
						throw runtime_error("Expected ';' but found '" + str + "'.");
					
					ExpVal val = readExpression(comp, pComma);
					if(val.getType().getID() == ident.id)
						comp.warn(Warning::UnnecessaryCast, "Casting to the same type.");
					else
						val = comp.getConvs().convert(val, Type(ident.id), true);
							
					readLexem(comp, LexemType::RightBracket, ")");
					return readPostfix(comp, std::move(val));
				}
				default:
					throw runtime_error("Expected expression but found '" + str + "'.");
			}
		}
		case LexemType::LeftBracket: {
			auto val = readExpression(comp);
			readLexem(comp, LexemType::RightBracket, ")");
			return readPostfix(comp, std::move(val));
		}
		default:
			throw runtime_error("Expected expression but found '" + string(str) + "'.");
	}
}

ExpVal readOperator(Compiler & comp, ExpVal & a) {
	auto[t, str] = comp.getToken();
	
	a.keep();
	ExpVal b = readExpression(comp, getOpPri(str[0]));
	a.keep(false);
	return comp.getOperators().getBinary(str[0]).eval(a, b);
}

ExpVal readExpression(Compiler & comp, Priority priority) {
	ExpVal val = readExpMem(comp);
	
	auto[t, str] = comp.peekToken();
	while(true) {
		switch(t) {
			case LexemType::Assign: {
				if(priority >= pAssign)
					return val;
				if(val.getLoc() != ExpValLoc::GlobVar && val.getLoc() != ExpValLoc::LocalVar && !val.isReference())
					throw runtime_error("Can't assign to RValue.");
				
				comp.getToken();
				
				val.keep();
				ExpVal val2 = readExpression(comp, pAssign - 1);
				val.keep(false);
				
				assign(comp, val, val2);
				// Optimization, val is likely in pri and needs not to be loaded again
				if(comp.getMemMngr().isInPri(val2, false))
					comp.getMemMngr().setPri(val, false);
				else if(comp.getMemMngr().isInPri(val2, true))
					comp.getMemMngr().setPri(val, true);
				break;
			}
			case LexemType::Operator:
				if(priority >= getOpPri(str[0]))
					return  val;
				
				val = readOperator(comp, val);
				break;
			case LexemType::Comma:
				if(priority >= pComma)
					return val;
				
				comp.getToken();
				return readExpression(comp, priority);
			default:
				return val;
		}
		std::tie(t, str) = comp.peekToken();
	}
}

void skipStatement(Compiler & comp) {
	auto [t, str] = comp.getToken();
	if(t == LexemType::LeftCurly) {
		uint n = 1;
		do {
			std::tie(t, str) = comp.getToken();
			if(t == LexemType::LeftCurly)
				++n;
			else if(t == LexemType::RightCurly)
				--n;
		}
		while(n);
	}
	else {
		while(t != LexemType::Semicolon)
			std::tie(t, str) = comp.getToken();
	}
}

bool readIf(Compiler & comp, Type & ret_type, uint brk, uint cnt) {
	auto [t, str] = comp.getToken();
	if(t != LexemType::If)
		throw runtime_error("Expected if but found '" + str + ";.");
	
	bool isConstexpr = false;
	
	auto [t2, str2] = comp.peekToken();
	if(t2 == LexemType::Constexpr) {
		comp.getToken();
		isConstexpr = true;
	}
	
	readLexem(comp, LexemType::LeftBracket, "(");
	ExpVal val = readExpression(comp);
	if(val.getType() != BasicType::Bool && val.getType() != BasicType::Int)
		val = comp.getConvs().convert(val, Type(BasicType::Bool));
	readLexem(comp, LexemType::RightBracket, ")");
	
	if(isConstexpr) {
		if(!val.isConstexpr())
			throw runtime_error("Expression must be constexpr.");
		
		if(val.getCell()) {
			bool ret = readStatement(comp, ret_type, brk, cnt);
			
			std::tie(t, str) = comp.peekToken();
			if(t == LexemType::Else) {
				comp.getToken();
				skipStatement(comp);
			}
			return ret;
		}
		
		skipStatement(comp);
		std::tie(t, str) = comp.peekToken();
		if(t == LexemType::Else) {
			comp.getToken();
			return readStatement(comp, ret_type, brk, cnt);
		}
		return false;
	}
	
	if(val.isConstexpr())
		comp.warn(Warning::ConstexprIf, "Constant expression in if. Use 'if constexpr' instead.");
		
	toPri(val);
	uint lelse = comp.makeLabel();
	comp.emit(Opcode::op_jzer, lelse);
	bool ret = readStatement(comp, ret_type, brk, cnt);
		
	std::tie(t, str) = comp.peekToken();
	if(t == LexemType::Else) {
		comp.getToken();
		uint lend = comp.makeLabel();
		comp.emit(Opcode::op_jump, lend);
		comp.fillLabelNext(lelse);
		ret &= readStatement(comp, ret_type, brk, cnt);
		comp.fillLabelNext(lend);
		return ret;
	}
	comp.fillLabelNext(lelse);
	return false;
}

void readLocalVars(Compiler & comp) {
	Type type = getType(comp);
	
	if(type.isNative())
		throw runtime_error("Can't have native variable.");
	if(type.isAutoRef())
		throw runtime_error("Can't have auto ref variable.");
	
	LexemType t;
	std::string str;
	
	do {
		std::tie(t, str) = comp.getToken();
		if(t != LexemType::Identifier)
			throw runtime_error("Expected identifier but found '" + string(str) + "'.");
		
		auto[t2, str2] = comp.peekToken();
		if(t2 == LexemType::LeftSquare) {
			comp.getToken();
			ExpVal count = readExpression(comp);
			if(!count.isConstexpr())
				throw runtime_error("Array size must be constexpr.");
			if(count.getType() != BasicType::Int)
				throw runtime_error("Array size must be int.");
			
			readLexem(comp, LexemType::RightSquare, "]");
			
			comp.addLocalArray(str, type, count.getCell());
		}
		else {
			ExpVal val = getInitValue(comp);
			if(type.isAuto()) {
				if(val.getType() == BasicType::Void)
					throw runtime_error("Type can't be deduced.");
				if(!type.acceptsAuto(val.getType()))
					throw runtime_error("Type mismatch.");
				
				type.autoDeduce(val.getType());
				if(val.isConstexpr())
					type.removeFlag(VariableFlags::Constexpr);
				comp.addLocalVariable(str, type, val);
			}
			else {
				if(val.getType() != BasicType::Void && !type.accepts(val.getType()))
					val = comp.getConvs().convert(val, type);
				
				comp.addLocalVariable(str, type, val);
			}
		}
		std::tie(t, str) = comp.getToken();
	} while(t == LexemType::Comma);
	
	if(t != LexemType::Semicolon)
		throw runtime_error("Expected ';' but found '" + string(str) + "'.");
}

bool readWhile(Compiler & comp, Type & ret_type) {
	readLexem(comp, LexemType::While, "while");
	
	uint start = comp.makeLabel();
	uint end = comp.makeLabel();
	comp.fillLabelNext(start);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	
	readLexem(comp, LexemType::LeftBracket, "(");
	ExpVal val = readExpression(comp);
	if(val.getType() != BasicType::Bool && val.getType() != BasicType::Int)
		val = comp.getConvs().convert(val, Type(BasicType::Bool));
	readLexem(comp, LexemType::RightBracket, ")");
	
	toPri(val);
	comp.emit(Opcode::op_jzer, end);
	readStatement(comp, ret_type, end, start);
		
	comp.emit(Opcode::op_jump, start);
	
	comp.fillLabelNext(end);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	return false;
}

bool readDoWhile(Compiler & comp, Type & ret_type) {
	readLexem(comp, LexemType::Do, "do");
	
	uint start = comp.makeLabel();
	uint end = comp.makeLabel();
	uint cond = comp.makeLabel();
	comp.fillLabelNext(start);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	
	readStatement(comp, ret_type, end, cond);
	
	comp.fillLabelNext(cond);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	
	readLexem(comp, LexemType::While, "while");
	readLexem(comp, LexemType::LeftBracket, "(");
	ExpVal val = readExpression(comp);
	if(val.getType() != BasicType::Bool && val.getType() != BasicType::Int)
		val = comp.getConvs().convert(val, Type(BasicType::Bool));
	readLexem(comp, LexemType::RightBracket, ")");
	readLexem(comp, LexemType::Semicolon, ";");
	
	toPri(val);
	comp.emit(Opcode::op_jnz, start);
	comp.fillLabelNext(end);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	return false;
}

bool readFor(Compiler & comp, Type & ret_type) {
	readLexem(comp, LexemType::For, "for");
	
	readLexem(comp, LexemType::LeftBracket, "(");
	
	auto [t, str] = comp.peekToken();
	if(t == LexemType::Identifier && comp.getIdent(str).type == IdentifierType::Type)
		readStatement(comp, ret_type);
	else
		readExpression(comp);
	
	uint start = comp.makeLabel();
	uint end = comp.makeLabel();
	comp.fillLabelNext(start);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	ExpVal val = readExpression(comp);
	if(val.getType() != BasicType::Bool && val.getType() != BasicType::Int)
		val = comp.getConvs().convert(val, Type(BasicType::Bool));
	
	toPri(val);
	comp.emit(Opcode::op_jzer, end);
	
	readLexem(comp, LexemType::Semicolon, ";");
	comp.newCode();
	readExpression(comp);
	Code code = comp.releaseCode();
	readLexem(comp, LexemType::RightBracket, ")");
	
	readStatement(comp, ret_type, end, start);
	comp.code() << code;
	
	comp.emit(Opcode::op_jump, start);
	
	comp.fillLabelNext(end);
	comp.getMemMngr().clrPri();
	comp.getMemMngr().clrAlt();
	return false;
}

bool readStatement(Compiler & comp, Type & ret_type, uint brk, uint cnt) {
	auto [t, str] = comp.peekToken();
	switch(t) {
		case LexemType::LeftCurly:
			comp.getToken();
			return readBlock(comp, ret_type, brk, cnt);
		case LexemType::If:
			return readIf(comp, ret_type, brk, cnt);
		case LexemType::While:
			return readWhile(comp, ret_type);
		case LexemType::Do:
			return readDoWhile(comp, ret_type);
		case LexemType::For:
			return readFor(comp, ret_type);
		case LexemType::Break:
			if(brk == 0)
				throw runtime_error("No loop to break out of.");
			comp.getToken();
			comp.emit(Opcode::op_jump, brk);
			readLexem(comp, LexemType::Semicolon, ";");
			break;
		case LexemType::Continue:
			if(cnt == 0)
				throw runtime_error("No loop to break out of.");
			comp.getToken();
			comp.emit(Opcode::op_jump, cnt);
			readLexem(comp, LexemType::Semicolon, ";");
			break;
		case LexemType::Const:
			readLocalVars(comp);
			break;
		case LexemType::Operator:
		case LexemType::UnOperator:
			readExpression(comp);
			readLexem(comp, LexemType::Semicolon, ";");
			break;
		case LexemType::Identifier: {
			Identifier ident = comp.getIdent(str);
			switch(ident.type) {
				case IdentifierType::Type:
					readLocalVars(comp);
					break;
				case IdentifierType::GlobVar:
				case IdentifierType::LocalVar:
				case IdentifierType::Func:
				case IdentifierType::FuncArg:
					readExpression(comp);
					readLexem(comp, LexemType::Semicolon, ";");
					break;
				default:
					throw runtime_error("Unexpected identifier '" + str + "'.");
			}
			break;
		}
		case LexemType::Return: {
			comp.getToken(); // Discard peeked token
			std::tie(t, str) = comp.peekToken();
				
			ExpVal val(BasicType::Void, 0);
			if(t != LexemType::Semicolon)
				val = readExpression(comp);
			
			readLexem(comp, LexemType::Semicolon, ";");
			if(ret_type.isAuto()) {
				if(!ret_type.acceptsAuto(val.getType()))
					throw runtime_error("Auto can't accept this type.");
				ret_type.autoDeduce(val.getType());
			}
			else if(!ret_type.accepts(val.getType()))
				val = comp.getConvs().convert(val, Type(ret_type.getID()));
			
			if(val.getType() != BasicType::Void && !val.isInPri())
				val.toPri();
			comp.freeStack();
			comp.emit(Opcode::op_retn);
			return true;
		}
		default:
			throw runtime_error("Expected statement but found '" + string(str) + "'.");
	}
	return false;
}

bool readBlock(Compiler & comp, Type & ret_type, uint brk, uint cnt) {
	comp.pushLayer();
	
	bool ret = false;
	auto [t, str] = comp.peekToken();
	while(t != LexemType::RightCurly) {
		if(ret)
			comp.warn(Warning::Unreachable, "Unreachable code.");
		ret |= readStatement(comp, ret_type, brk, cnt);
		std::tie(t, str) = comp.peekToken();
	}
	comp.getToken();
	comp.popLayer(!ret);
	return ret;
}

void readFunc(Compiler & comp, Function & func) {
	auto [t, str] = comp.peekToken();
	switch(t) {
		case LexemType::Semicolon:
			comp.getToken();
			comp.clearFuncArgs();
			break;
		case LexemType::Return:
		case LexemType::LeftCurly: {
			func.pos = comp.emit(Opcode::op_proc);
			comp.pushLayer();
			
			bool ret = readStatement(comp, func.type);
			
			comp.popLayer(!ret);
			if(!ret) {
				if(func.type != BasicType::Void)
					comp.warn(Warning::ShouldReturn, "Function should return a value.");
				comp.emit(Opcode::op_retn);
			}
			comp.clearFuncArgs();
			break;
		}
		default: {
			func.pos = comp.emit(Opcode::op_proc);
			comp.pushLayer();
			
			readExpression(comp);
			
			comp.popLayer(true);
			if(func.type != BasicType::Void)
				comp.warn(Warning::ShouldReturn, "Function should return a value.");
			comp.emit(Opcode::op_retn);
			comp.clearFuncArgs();
			break;
		}
	}
}

bool readFuncArg(Compiler & comp, Function & func, uint pos, uint & offset, bool check) {
	Type type = getType(comp);
			
	auto [t, str] = comp.getToken();
	if(t == LexemType::Dots)
		type.addFlag(VariableFlags::VarArgs);
	else if(t != LexemType::Identifier)
		throw runtime_error("Expected identifier but found '" + string(str) + "'.");
	
	if(check) {
		if(func.args.size() <= pos)
			throw runtime_error("Function definition does not match declaration (too many args).");
		
		Argument & arg = func.args[pos];
		if(arg.type != type || arg.name != str)
			throw runtime_error("Function definition does not match declaration (arg " + std::to_string(pos) + ").");
		
		comp.addFuncArg(&arg);
	}
	else {
		auto & args = func.args;
		args.emplace_back(str, offset, type);
		offset += comp.getTypeInfo(args.back().type).size;
	}
	return t != LexemType::Dots;
}

void readFuncArgs(Compiler & comp, Function & func, bool check) {
	uint flags = 0;
	auto [t, str] = comp.peekToken();
	if(t == LexemType::RightBracket) {
		comp.getToken();
		return;
	}
	
	uint pos = 0;
	uint offset = 12;
	readFuncArg(comp, func, pos, offset, check);
	
	tie(t, str) = comp.getToken();
	bool cont = true;
	while(cont && t == LexemType::Comma) {
		++pos;
		if(func.args.back().type.isAuto())
			throw runtime_error("A named function argument can't be auto.");
		cont = readFuncArg(comp, func, pos, offset, check);
		tie(t, str) = comp.getToken();
	}
	
	if(cont && !func.args.empty() && func.args.back().type.isAuto())
		throw runtime_error("A named function argument can't be auto.");
	
	if(check && ++pos != func.args.size())
		throw runtime_error("Function definition does not match declaration (missing args).");

	if(t != LexemType::RightBracket)
		throw runtime_error("Expected ')' but found '" + string(str) + "'.");
	
	for(auto & i : func.args)
		comp.addFuncArg(&i);
}

void readVar(Compiler & comp, Type type, std::string name) {
	auto [t, str] = comp.peekToken();
	if(t == LexemType::Assign) {
		comp.getToken(); // discard peeked assign
		ExpVal val = readExpression(comp, pComma);
		if(!val.isConstexpr())
			throw runtime_error("Expression must be constexpr.");
		
		if(type.isAuto()) {
			if(!type.acceptsAuto(val.getType()))
				throw runtime_error("Type mismatch.");
			type.autoDeduce(val.getType());
		}	
		else if(!type.accepts(val.getType())) {
			val = comp.getConvs().convert(val, type);
			if(!val.isConstexpr())
				throw runtime_error("Can't convert constexpr expression.");
		}
		
		comp.addGlobalVariable(std::move(name), type, val.getCell());
	}
	else if(type.isAuto())
		throw runtime_error("Can't deduce type.");
	else if(t == LexemType::LeftSquare) {
		comp.getToken();
		ExpVal count = readExpression(comp);
		readLexem(comp, LexemType::RightSquare, "]");
		if(!count.isConstexpr())
			throw runtime_error("Array size must be constexpr.");
		comp.addGlobalArray(std::move(name), type, count.getCell());
	}
	else
		comp.addGlobalVariable(std::move(name), type, 0);
}

void readOperatorDef(Compiler & comp, const Type & type) {
	auto [t, str] = comp.getToken();
	if(t == LexemType::Assign) {
		readLexem(comp, LexemType::LeftBracket, "(");
		uint offset;
		Function func("", type, 0);
		readFuncArg(comp, func, 0, offset, false);
		readLexem(comp, LexemType::RightBracket, ")");
		
		if(type.isNative()) {
			bool implicit = true;
			readLexem(comp, LexemType::Assign, "=");
			tie(t, str) = comp.getToken();
			if(t == LexemType::Explicit) {
				implicit = false;
				tie(t, str) = comp.getToken();
			}
			
			if(t != LexemType::Identifier)
				throw runtime_error("Expected native but found '" + str + "'.");
			auto ident = comp.getIdent(str);
			if(ident.type != IdentifierType::Func)
				throw runtime_error("Expected native but found '" + str + "'.");
			auto & cfunc = comp.getFunc(ident.id);
			if(!cfunc.type.isNative())
				throw runtime_error("Expected native but found '" + str + "'.");
			
			comp.getConvs().addConversionFunc(func.args.back().type.getID(), type.getID(), ident.id, implicit);
			readLexem(comp, LexemType::Semicolon, ";");
			return;
		}
		tie(t, str) = comp.getToken();
		if(t == LexemType::Assign) {
			tie(t, str) = comp.getToken();
			bool implicit = true;
			if(t == LexemType::Explicit) {
				implicit = false;
				tie(t, str) = comp.getToken();
			}
			
			if(t == LexemType::Const)
				comp.getConvs().addConversionNop(func.args.back().type.getID(), type.getID(), implicit);
			else if(t == LexemType::Identifier) {
				auto ident = comp.getIdent(str);
				if(ident.type != IdentifierType::Func)
					throw runtime_error("Expected function but found '" + str + "'.");
				auto & cfunc = comp.getFunc(ident.id);
				comp.getConvs().addConversionFunc(func.args.back().type.getID(), type.getID(), ident.id, implicit);
			}
			readLexem(comp, LexemType::Semicolon, ";");
			return;
		}
	}
	throw runtime_error("Invalid operator definition.");
}

void readVarFunc(Compiler & comp, const Type & type) {
	auto [t, str] = comp.getToken();
	if(t == LexemType::OperatorFunc)
		return readOperatorDef(comp, type);
	
	if(t != LexemType::Identifier)
		throw runtime_error("Expected identifier but found '" + string(str) + "'.");
	
	auto [t2, str2] = comp.peekToken();
	if(t2 == LexemType::LeftBracket) {
		auto ident = comp.getIdent(str);
		uint id;
		if(ident.type == IdentifierType::Unknown)
			id = comp.addFunc(str, type);
		else if(ident.type == IdentifierType::Func) {
			id = ident.id;
			Function & func = comp.getFunc(id);
			if(func.type != type)
				throw runtime_error("Function header differs from definition.");
			if(func.pos != 0)
				throw runtime_error("Function already defined.");
			if(type.isNative())
				throw runtime_error("Native function already declared.");
			if(type.isAutoRef())
				throw runtime_error("Function can't return auto ref.");
		}
		else
			throw runtime_error("Name already in use.");
		
		comp.getToken(); // discard peeked '('
		readFuncArgs(comp, comp.getFunc(id), ident.type == IdentifierType::Func);
		if(type.isNative())
			readLexem(comp, LexemType::Semicolon, ";");
		else
			readFunc(comp, comp.getFunc(id));
	}
	else {
		if(type.isNative())
			throw runtime_error("Can't have native variable.");
		if(type.isAutoRef())
			throw runtime_error("Can't have auto ref variable.");
		
		readVar(comp, type, std::move(str));
		
		std::tie(t2, str2) = comp.getToken();
		while(t2 == LexemType::Comma) {
			std::tie(t, str) = comp.getToken();
			if(t != LexemType::Identifier)
				throw runtime_error("Expected <identifier> but found '" + string(str) + "'.");
					
			readVar(comp, type, str);
			std::tie(t2, str2) = comp.getToken();
		}
		if(t2 != LexemType::Semicolon)
			throw runtime_error("Expected ';' but found '" + str + "'.");
	}
}

void readType(Compiler & comp) {
	Type type = getType(comp);
	readVarFunc(comp, type);
}

void readTypedef(Compiler & comp) {
	readLexem(comp, LexemType::Typedef, "typedef");
	auto [t2, name] = comp.getToken();
	if(t2 != LexemType::Identifier)
		throw runtime_error("Identifier expected but found '" + name + "'.");
	if(comp.getIdent(name).type != IdentifierType::Unknown)
		throw runtime_error("Identifier '" + name + "' already exists.");
	
	auto[t, str] = comp.getToken();
	if(t == LexemType::Assign) {
		tie(t, str) = comp.getToken();
		bool implicit = true;
		if(t == LexemType::Explicit) {
			implicit = false;
			tie(t, str) = comp.getToken();
		}
		
		if(t != LexemType::Identifier)
			throw runtime_error("Identifier expected but found '" + str + "'.");
		auto ident = comp.getIdent(str);
		if(ident.type != IdentifierType::Type)
			throw runtime_error("Type expected but found '" + str + "'.");
		
		readLexem(comp, LexemType::Semicolon, ";");
		comp.addType(std::move(name), ident.id, implicit);
		return;
	}
	if(t != LexemType::Semicolon)
		throw runtime_error("Expected ';' but found '" + str + "'.");
	comp.addType(std::move(name), 0);
}

void readGlobal(Compiler & comp) {
	auto [t, str] = comp.peekToken();
	while(true) {
		switch(t) {
			case LexemType::Const:
			case LexemType::Constexpr:
			case LexemType::Public:
			case LexemType::Native:
			case LexemType::Identifier:
				readType(comp);
				break;
			case LexemType::Include:
				comp.getToken();
				comp.pushInput(readStringPeek(comp));
				comp.getToken();
				break;
			case LexemType::EndOfFile:
				if(!comp.popInput())
					return;
				comp.getToken();
				break;
			case LexemType::PragmaDynamic: {
				comp.getToken();
				ExpVal val = readExpression(comp, pComma);
				if(!val.isConstexpr())
					throw runtime_error("Value must be constexpr.");
				if(val.getType() != BasicType::Int)
					throw runtime_error("Value must be int.");
				
				comp.getOptions().setStackSize(val.getCell());
				break;
			}
			case LexemType::Typedef:
				readTypedef(comp);
				break;
			default:
				throw runtime_error("Unexpected '" + string(str) + "' in global scope.");
		}
		std::tie(t, str) = comp.peekToken();
	}
}

Compiler::Compiler() noexcept{
	types_.push_back(TypeInfo{"void", 0});
	idents_.addGlobIdent("void", IdentifierType::Type, 0);
	types_.push_back(TypeInfo{"int", 4});
	idents_.addGlobIdent("int", IdentifierType::Type, 1);
	types_.push_back(TypeInfo{"float", 4});
	idents_.addGlobIdent("float", IdentifierType::Type, 2);
	types_.push_back(TypeInfo{"bool", 4});
	idents_.addGlobIdent("bool", IdentifierType::Type, 3);
	types_.push_back(TypeInfo{"auto", 4});
	idents_.addGlobIdent("auto", IdentifierType::Type, 4);
	types_.push_back(TypeInfo{"any_pointer", 4});

	natFunc_.floatadd = addFunc("floatadd", Type(BasicType::Float, VariableFlags::Native));
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Float));
	lastFunc().args.emplace_back("b", 16, Type(BasicType::Float));
	natFunc_.floatsub = addFunc("floatsub", Type(BasicType::Float, VariableFlags::Native));
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Float));
	lastFunc().args.emplace_back("b", 16, Type(BasicType::Float));
	natFunc_.floatmul = addFunc("floatmul", Type(BasicType::Float, VariableFlags::Native));
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Float));
	lastFunc().args.emplace_back("b", 16, Type(BasicType::Float));
	natFunc_.floatdiv = addFunc("floatdiv", Type(BasicType::Float, VariableFlags::Native));
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Float));
	lastFunc().args.emplace_back("b", 16, Type(BasicType::Float));
	natFunc_.floatcmp = addFunc("floatcmp", Type(BasicType::Float, VariableFlags::Native));
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Float));
	lastFunc().args.emplace_back("b", 16, Type(BasicType::Float));
	natFunc_.floatctr = funcs_.size();
	funcs_.emplace_back("float", Type(BasicType::Float, VariableFlags::Native), 0);
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Int));
	natFunc_.floatround = addFunc("floatround", Type(BasicType::Int, VariableFlags::Native));
	lastFunc().args.emplace_back("a", 12, Type(BasicType::Float));
	lastFunc().args.emplace_back("method", 16, Type(BasicType::Int));
}

void Compiler::parse() {
	try {
		getToken();
		readGlobal(*this);
	}
	catch(const exception & e) {
		throw runtime_error("Error in file \"" + get<1>(files_.back()) + "\" line " + std::to_string(line_) + ": " + e.what());
	}
}

template<typename T>
class ComparablePtr {
	T* ptr;
	
public:
	ComparablePtr(T& f) : ptr(&f) {};
	ComparablePtr(T* f) : ptr(f) {};
	
	T* operator->() { return ptr; }
	T& operator*() { return *ptr; }
	
	const T* operator->() const { return ptr; }
	const T& operator*() const { return *ptr; }
	
	bool operator<(const ComparablePtr & f) const { return ptr->name < f->name; }
};

void Compiler::write(std::ostream & os) {
	amx_file_writer writer(os);
	amx_file_data header;
	literals_.finalize();
	
	header.publics = amx_file_data::header_size;
	header.natives = header.publics;
	header.libraries = header.natives;
	
	// Count sizes of public and native tables
	for(const auto & i : funcs_) {
		if(i.type.isPublic()) {
				header.natives += 8;
				header.libraries += 8;
		}
		else if(i.type.isNative())
			header.libraries += 8;
	}

	header.pubvars = header.libraries;
	header.tags = header.pubvars;
	
	for(const auto & i : globVars_) {
		if(i.type().isPublic())
			header.tags += 8;
	}
	
	header.nametable = header.tags;
	
	vector<ComparablePtr<Function>> sortedPublics;
	vector<ComparablePtr<const GlobalVariable>> sortedPublicVars;
	// Size of nametable
	header.cod = header.nametable + 2;
	for(auto & i : funcs_) {
		if(i.type.isNative())
			header.cod += i.name.length() + 1;
		else if(i.type.isPublic()) {
			sortedPublics.emplace_back(&i);
			header.cod += i.name.length() + 1;
		}
		
		if(i.name == "main")
			header.cip = i.pos * 4;
	}
	for(const auto & i : globVars_) {
		if(i.type().isPublic()) {
			sortedPublicVars.emplace_back(&i);
			header.cod += i.name.length() + 1;
		}
	}
	while((header.cod % 4) != 0)
		++header.cod;
	
	header.dat = header.cod + 4 * code_.size();
	header.hea = header.dat + literals_.size();
	header.stp = header.hea + options_.getStackSize();
	header.size = header.hea;
	
	// Write Header
	header.write(writer);
	
	// Sort
	sort(sortedPublics.begin(), sortedPublics.end());
	
	// Write publics table
	unsigned int loc = header.nametable + 2;
	for(const auto & i : sortedPublics) {
		writer << (i->pos * 4);
		writer << loc;
		loc += i->name.length() + 1;
	}
	
	// Write natives table
	unsigned int nat_pos = 0;
	for(auto & i : funcs_) {
		if(!i.type.isNative())
			continue;
		writer << 0u;
		writer << loc;
		
		i.pos = nat_pos;
		++nat_pos;
		loc += i.name.length() + 1;
	}
	
	for(const auto & i : sortedPublicVars) {
		writer << (i->pos * 4);
		writer << loc;
		loc += i->name.length() + 1;
	}
	
	// Write nametable max len
	writer << static_cast<unsigned short>(0x1F);
	
	// Write nametable
	for(const auto & i : sortedPublics) {
		writer << i->name;
	}
	for(const auto & i : funcs_) {
		if(i.type.isNative())
			writer << i.name;
	}
	for(const auto & i : sortedPublicVars) {
		writer << i->name;
	}
	
	// Padding for nametable
	while((loc % 4) != 0) {
		writer << static_cast<unsigned char>(0);
		++loc;
	}
		
	// Write instructions
	const vector<cell>& insts = code_.getInsts();
	const vector<uint>& jumps = code_.getJumps();
	for(size_t i = 0, size = insts.size(); i < size; ++i) {
		unsigned int op = insts[i];
		writer << op;
		
		switch(Opcodes::opcodelist[op].ptype) {
			case Opcodes::ParamType::Function: {
				const Function & func = funcs_[insts[++i]];
				if(func.pos == 0)
					throw runtime_error("Function '" + func.name + "' declared but not defined.");
				writer << (func.pos * 4);
				break;
			}
			case Opcodes::ParamType::Native: {
				const Function & func = funcs_[insts[++i]];
				writer << func.pos;
				break;
			}
			case Opcodes::ParamType::Label: {
				uint lab = jumps[insts[++i]];
				if(lab == 0)
					throw runtime_error("Label not defined.");
				writer << 4 * lab;
				break;
			}
			default:
				for(size_t j = 0; j < Opcodes::opcodelist[op].params; ++j) {
					writer << insts[++i];
				}
				break;
		}
	}
	
	for(const auto & i : literals_)
		writer << i;
}



