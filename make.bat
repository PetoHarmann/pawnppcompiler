@echo off

win_flex --wincompat --noline pawn++.l
mingw32-g++ lex.yy.c compiler.cpp opcodes.cpp types.cpp types_conv.cpp exp_val.cpp types_ops.cpp idents.cpp local_vars.cpp code.cpp -std=c++17 -IC:\MinGW\include -o pawnpp
